#! /usr/bin/env python
# coding=utf-8
# -*- coding: utf-8 -*-
# date 2017/1/24

"""
功能：

1.0 2017/1/24 完成基本功能


"""
import sys
import platform
import wx
import os
from Cjx.file import CjxFile

#from os import listdir
#from os.path import isfile, join

from Views.Main import MainWindow
from ReplaceFactory.StringReplace import StringReplace

__author__ = "chenjiaxin <selectersky@qq.com>"
__version__ = "1.0"
__charset__ = "utf-8"


class MyFrame(wx.Frame):
    """ We simply derive a new class of Frame. """
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=(300, 500))
        self.control = wx.TextCtrl(self, style=wx.TE_MULTILINE)
        self.Show(True)

def main():
    """
     main
     """
    # app = wx.App(False)
    # frame = MainWindow(None,  "Hell world")
    # app.MainLoop()
    #a = eval('StringReplace("/d/web",{"sf":"13"})')
    #a.process("sfsf")
    mypath = "/d/android/"
    onlyfiles = CjxFile().files(mypath, True, "py|md|java|xml")

    print onlyfiles

if __name__ == '__main__':
    reload(sys)
    if platform.system() == u'Windows':
        __charset__ = u'gb2312'
    sys.setdefaultencoding(__charset__)

    main()