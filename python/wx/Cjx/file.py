#! /usr/bin/env python
# coding=utf-8
# -*- coding: utf-8 -*-
# date 2017/1/25
# author chenjiaxin
# version:1.0

"""
功能：

"""

import os
import re
import shutil
from tempfile import mkstemp

class CjxFile:
    defaultExinclude = None
    def __init__(self, defaultExinclude = ".git|.DS_Store|.svn|.idea"):
            self.defaultExinclude = defaultExinclude

    def files(self, path_, sub_=True, exts_=None, exinclude_=None):
        """
        返回指定目录的文件列表

        :param path_: 路径
        :param sub_: 是否查找子目录
        :param exts_: 包括扩展名
        :param exinclude_:
        :return: 路径数组
        """
        lists = []
        reExts= "|"+exts_+"|"
        for row in os.listdir(path_):
            f = os.path.join(path_, row)
            if self.is_exinclude(f, exinclude_):
                continue

            if os.path.isfile(f):
                _, file_extension = os.path.splitext(f)
                ext = "|"+file_extension[1:]+"|"
                if exts_:
                    # 匹配扩展名
                    if reExts.find(ext) != -1:
                        lists.append(f)
                else:
                        lists.append(f)
            else:
                if sub_:
                    l = self.files(f, sub_, exts_, exinclude_)
                    for i in l:
                        lists.append(i)
        return lists

    def is_exinclude(self, str, exincldue_=None):

        if not exincldue_ and not self.defaultExinclude:
            return False

        exinclude = ""

        if exincldue_:
            exinclude = exincldue_

        if self.defaultExinclude:
            exinclude = exinclude + "|" + self.defaultExinclude

        for s in exinclude.split("|"):
            if len(s) < 1:
                continue
            if str.find(s) != -1:
                # print 'find:%s,%s'%(str, s)
                return True
        return False

    def sed(self, pattern, replace, source, dest=None, count=0):
        """Reads a source file and writes the destination file.

        In each line, replaces pattern with replace.

        Args:
            pattern (str): pattern to match (can be re.pattern)
            replace (str): replacement str
            source  (str): input filename
            count (int): number of occurrences to replace
            dest (str):   destination filename, if not given, source will be over written.
        """

        fin = open(source, 'r')
        num_replaced = count

        if dest:
            fout = open(dest, 'w')
        else:
            fd, name = mkstemp()
            fout = open(name, 'w')

        for line in fin:
            out = re.sub(pattern, replace, line)
            fout.write(out)

            if out != line:
                num_replaced += 1
            if count and num_replaced > count:
                break
        try:
            fout.writelines(fin.readlines())
        except Exception as E:
            raise E

        fin.close()
        fout.close()

        if not dest:
            shutil.move(name, source)