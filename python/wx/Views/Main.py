#! /usr/bin/env python
# coding=utf-8
# -*- coding: utf-8 -*-
# date 2017/1/24

"""
功能：

1.0 2017/1/24 完成基本功能


"""

import wx
import os

__author__ = "chenjiaxin <selectersky@qq.com>"
__version__ = "1.0"
__charset__ = "utf-8"


class MainWindow(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=(500,500))
        self.control = wx.TextCtrl(self, style=wx.TE_MULTILINE)
        self.CreateStatusBar() # A Statusbar in the bottom of the window

        # Setting up the menu.
        filemenu  = wx.Menu()

        # wx.ID_ABOUT and wx.ID_EXIT are standard IDs provided by wxWidgets.
        menuitem = filemenu.Append(wx.ID_ABOUT, "&About"," Information about this program")
        self.Bind(wx.EVT_MENU, self.onAbout, menuitem)
        menuitem = filemenu.Append(wx.ID_OPEN, u"打开", u"打开一个文件")
        self.Bind(wx.EVT_MENU, self.onOpen, menuitem)
        filemenu.AppendSeparator()
        filemenu.Append(wx.ID_EXIT,"E&xit"," Terminate the program")

        # Creating the menubar.
        menuBar = wx.MenuBar()
        menuBar.Append(filemenu,"&File") # Adding the "filemenu" to the MenuBar
        self.SetMenuBar(menuBar)  # Adding the MenuBar to the Frame content.
        self.Show(True)

    def onAbout(self, event):
        dlg = wx.MessageDialog(self,"A small text editor", "about simple Editor", wx.OK)
        dlg.ShowModal()
        dlg.Destroy()

    def onOpen(self, event):
        self.dirname = ''
        dlg = wx.FileDialog(self, "选择一个文件", self.dirname, "", "*.*", wx.FD_OPEN)
        if dlg.ShowModal() == wx.ID_OK:
            self.filename = dlg.GetFilename()
            self.dirname = dlg.GetDirectory()
            f = open(os.path.join(self.dirname, self.filename), 'r')
            self.control.SetValue(f.read())
            f.close()
        dlg.Destroy()
