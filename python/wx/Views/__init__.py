#! /usr/bin/env python
# coding=utf-8
# -*- coding: utf-8 -*-
# date 2017/1/24

"""
功能：

1.0 2017/1/24 完成基本功能


"""
import sys
import platform

__author__ = "chenjiaxin <selectersky@qq.com>"
__version__ = "1.0"
__charset__ = "utf-8"


def main():
    """
     main
     """


if __name__ == '__main__':
    reload(sys)
    if platform.system() == u'Windows':
        __charset__ = u'gb2312'
    sys.setdefaultencoding(__charset__)
    main()