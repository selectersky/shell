#! /usr/bin/env python
# coding=utf-8
# -*- coding: utf-8 -*-
# date 2017/1/24
# author chenjiaxin
# version:1.0

"""
功能：

"""
import json
import os


class BaseFactory():
    datas = None
    types = None
    rootDir = None

    def __init__(self, root_dir, datas):
        self.datas = datas
        self.rootDir = root_dir
        fp = open('data/type.json', 'r')

        self.types = json.load(fp)
        fp.close()
        print self.types
        print "BaseFactory init"
        print self.types[0]["factory"]

    # def process(self, inputs):
    #     print "BaseFactory process"
    #     pass

    def data_by_id(self, id_):
        for row in self.types:
            if row["id"] == id_:
                return row
        return None