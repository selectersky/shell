#! /usr/bin/env python
# coding=utf-8
# -*- coding: utf-8 -*-
# date 2017/4/14
# author chenjiaxin
# version:1.0

import sys
import socket,struct

def _ip2int(ip):
    return socket.ntohl(struct.unpack("I", socket.inet_aton(str(ip)))[0])

def _int2ip(int_ip):
	return socket.inet_ntoa(struct.pack("I",socket.htonl(int_ip)))

def main():
    """
     main
    """
    if len(sys.argv) != 2:
    	print 'usage:ip 2030964265 or ip 1.1.1.1'
    	return

    v = sys.argv[1]
    if v.find('.')>0:
    	print _ip2int(v)
    else:
    	print _int2ip(int(v))

if __name__ == '__main__':
    main()