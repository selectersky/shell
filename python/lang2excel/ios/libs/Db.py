#! /usr/bin/env python
# coding=utf-8
# -*- coding: utf-8 -*-
# date 2017/4/10
# author chenjiaxin
# version:1.0

import json
import os
from Model.Rex import Rex
from Model.Template import Template


class Db:
    def __init__(self):
        self.rex_path = u're.json'
        self.template_path = u'template' + os.sep
        self.template_ext = u'.json'
        if not os.path.exists(self.template_path):
            os.mkdir(self.template_path)

        if not os.path.exists(self.rex_path):
            raise ValueError, u'正则模版文件不存在，path:%s'%self.rex_path


        j = self.json(self.rex_path)
        if not j or not isinstance(j, list):
            raise ValueError, u'读取正则模版文件失败，请确认文件格式正确，path:%s'%self.rex_path

        self.rexs = []
        for r in j:
            rex = Rex(r)
            self.rexs.append(rex)


    def templates(self):
        lists = []
        l = len(self.template_ext)
        for f in os.listdir(self.template_path):
            if f[-l:] == self.template_ext:
                lists.append(f[:-l])
        return lists

    def rex(self, rex_id):
        for r in self.rexs:
            if rex_id == r.id:
                return r
        return None

    def template(self, name):
        path = self.template_path + name + self.template_ext
        re = self.json(path)
        if re:
            v = Template(re)
            return v
        return re

    def json(self, path):
        """
        读取文件为json对象
        :param path:
        :return:
        """

        try:
            fp = open(path, 'r')
            body = fp.read()
            fp.close()
            j = json.loads(body)
            return j
        except:
            return None