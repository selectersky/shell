#! /usr/bin/env python
# coding=utf-8
# -*- coding: utf-8 -*-
# date 2017/4/10
# author chenjiaxin
# version:1.0

"""
功能：

"""
import re
import os
# import chardet

class LangFile:
    def __init__(self, path, encoding=u'utf-8'):
        self.path = path

        self.encoding = encoding
        if not os.path.exists(path):
            raise ValueError, u'资源文件不存在,path:%s'%path

        # # 自动检测文件编码
        # if not encoding:
        #     try:
        #         fp = open(path, 'r')
        #         body = fp.read()
        #         fp.close()
        #         info = chardet.detect(body)
        #         encode = info['encoding']
        #         if encode != 'utf-8':
        #             self.encoding = u'gbk'
        #     except:
        #         print u'资源文件打开失败,path:%s'%path

    def lines(self):
        try:
            fp = open(self.path, 'r')
            lines = fp.readlines()
            fp.close()
            return lines
        except:
            return None

    def decode(self, line, rex):
        result = re.search(rex, line)
        # 找了一条语言
        if result:
            key = result.group(1)
            value = result.group(2)
            return (key, value)
        return None

    def valueByKey(self, key):
        """
        根据key获取值
        :param key: 要查找的key
        :return: 如果没有找到，则返回None
        """
        lines = self.lines()
        if not lines:
            return None
        for line in lines:
            #print line
            line = line.decode(self.encoding)
            result = self.decode(line)
            #print line,result
            # 找了一条语言
            if result != None:
                k = result[0]
                value = result[1]
                if k == key:
                    return value
        return None
