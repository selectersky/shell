#! /usr/bin/env python
# coding=utf-8
# -*- coding: utf-8 -*-
# date 2017/4/10
# author chenjiaxin
# version:1.0

"""
功能：

"""


class Utils:
    def str(self, data, key):
        v = data.get(key, '')
        v = '%s' % v
        return v

    def arr(self, data, key):
        v = data.get(key, [])
        if not isinstance(v, list):
            v = []
        return v

    def dict(self, data, key):
        v = data.get(key, {})
        if not isinstance(v, dict):
            v = {}
        return v

    def int(self, data, key):
        v = data.get(key, 0)
        if isinstance(v, int):
            return v
        try:
            v = int(v)
            return v
        except:
            return 0

    def bool(self, data, key):
        v = data.get(key, False)
        if isinstance(v, bool):
            return v
        try:
            v = bool(v)
            return v
        except:
            return False