#! /usr/bin/env python
# coding=utf-8
# -*- coding: utf-8 -*-
# date 2017/4/10
# author chenjiaxin
# version:1.0

"""
功能：

"""
import openpyxl
import os

class Excel:
    def __init__(self, excel_path):
        self.encoding = u'gb2312'
        self.excel_path = excel_path
        if os.path.exists(excel_path):
            self.excel = openpyxl.load_workbook(excel_path)
        else:
            self.excel = openpyxl.Workbook()
        self.sheet = self.excel.active
        print u''

    def sheets_names(self):
        """

        :return: 获取所有sheet的名称
        """
        return self.excel.get_sheet_names()

    def use_sheet(self, name, auto_create=False):
        """

        :param name: sheet的标题
        :param auto_create: 是否自动创建sheep
        :return: Bool True 表示操作成功
        """
        try:
            self.sheet = self.excel.get_sheet_by_name(name)
            return True
        except:
            if auto_create:
                # 首先获取一下当前的sheet，如果当前sheet数据不为空，则重新创建一个sheet。否则直接使用当然的sheet
                self.sheet = self.excel.active
                if len(self.sheet.rows) > 0:
                    self.sheet = self.excel.create_sheet()
                    self.sheet.title = name
                    self.write(1, 1, u'key')
                    self.write(1, 2, u'中文')
                return True
            else:
                return False

    def remove_sheet(self, name):
        """
        删除sheet
        :param name:
        :return: 操作成功返回True
        """
        try:
            sheet = self.excel.get_sheet_by_name(name)
            self.excel.remove_sheet(sheet)
            return True
        except:
            return False

    def rows(self):
        """
        返回所有行
        :return:
        """
        return self.sheet.rows

    def row(self, key):
        """
        指定key返回指定的行
        :param key:
        :return: 如果key不存在，返回None
        """
        # 第一行为标题，自动忽略
        for i in range(2, len(self.rows()) + 1):
            k = self.read(i, 1)
            if key == k:
                return i
        return None

    def read(self, row, col):
        """
        读取指定行的值

        :param row:
        :param col:
        :return:
        """
        v = self.sheet.cell(row=row, column=col).value
        vv = v.decode(self.encoding)
        return vv

    def write(self, row, col, v):
        """
        设置指定行的值

        :param row:
        :param col:
        :param v:
        :return:
        """
        self.sheet.cell(row=row, column=col).value = v.encode(self.encoding)

    def append(self, key, value):
        """
        追加行

        :param key:
        :param value:
        :return:
        """

        self.sheet.append([key.encode(self.encoding), value.encode(self.encoding)])

    def save(self):
        self.excel.save(self.excel_path)

    # def valueByKey(self, key, lang):
    #     '''
    #     指定key返回Excel语言的值
    #     '''
    #     rows = len(self.table.rows)
    #     for row in range(2, len(self.table.rows) + 1):
    #         k = self.read(row, 1)
    #         if k == key:
    #             # 第一列是key，所以这里要加1
    #             value = self.read(row, lang + 1)
    #             if not value:
    #                 value = "None"
    #             return value
    #     return None