#! /usr/bin/env python
# coding=utf-8
# -*- coding: utf-8 -*-
# date 2017/4/10
# author chenjiaxin
# version:1.0
from libs.Utils import Utils
from Model.ToLang import ToLang

"""
功能：


"""

class SourceLang:
    def __init__(self, data={}):
        if isinstance(data, dict):
            u = Utils()
            self.path = u.str(data, 'path')
            self.is_file = u.bool(data, 'file')

            to = []
            for r in u.arr(data, 'to'):
                to_lang = ToLang(r)
                to.append(to_lang)
            self.to_lang = to
