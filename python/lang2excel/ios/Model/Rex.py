#! /usr/bin/env python
# coding=utf-8
# -*- coding: utf-8 -*-
# date 2017/4/10
# author chenjiaxin
# version:1.0
from libs.Utils import Utils
"""
功能：

"""


class Rex:
    def __init__(self, data={}):
        if isinstance(data, dict):
            u = Utils()
            self.id = u.int(data, 'id')
            self.name = u.str(data, 'name')
            self.rex = u.str(data, 'rex')