#! /usr/bin/env python
# coding=utf-8
# -*- coding: utf-8 -*-
# date 2017/4/10
# author chenjiaxin
# version:1.0
from libs.Utils import Utils
from Model.Module import Module
"""
功能：

"""
class Template:
    def __init__(self, data={}):
        if isinstance(data, dict):
            u = Utils()
            self.typeid = u.int(data, 'typeid')
            l = []
            for r in u.arr(data, 'modules'):
                m = Module(r)
                l.append(m)
            self.modules = l