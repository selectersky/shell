#! /usr/bin/env python
# coding=utf-8
# -*- coding: utf-8 -*-
# date 2017/4/10
# author chenjiaxin
# version:1.0
from libs.Utils import Utils
"""
功能：

"""
class ToLang:
    def __init__(self, data={}):
        if isinstance(data, dict):
            utils = Utils()
            self.name = utils.str(data, 'name')
            self.path = utils.str(data, 'path')