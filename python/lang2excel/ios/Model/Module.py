#! /usr/bin/env python
# coding=utf-8
# -*- coding: utf-8 -*-
# date 2017/4/10
# author chenjiaxin
# version:1.0

from Model.SourceLang import SourceLang
from libs.Utils import Utils

"""
功能：


"""

class Module:
    def __init__(self, data={}):
        if isinstance(data, dict):
            u = Utils()
            self.name = u.str(data, 'name')

            l = []
            for r in u.arr(data, 'sources'):
                source_lang = SourceLang(r)
                l.append(source_lang)
            self.sources = l
