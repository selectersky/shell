# ! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-
#date 16-5-23 3

"""
功能：自动将指定的ios语言资源导出到excel文档

1.0.0 2016.05.23 第一个版本完成 ，使用了xlwt插件，将语言资源导出到excel
1.0.1 2016.06.02 使用了openpyxl插件，支持excel 2012。支持检测语言资源是否存在的功能。
1.0.2 2016.06.15 全自动化导入和导出 
                 1）能自动删除excel里面不存在的键值 
                 2）手动写入的英文，能自动同步到excel中 
                 3）支持多语言 
                 4）英文里面书写的汉字(1个以上汉字)，同步时自动忽略，方便翻译
                 5）生成语言文件保留原来的注释和行数
1.0.3 2016.10.25 1）英文没有填写默认为中文 2）解决冲突，增加输入yy自动确认功能
"""

import openpyxl
import re
import os,platform
import sys

import libs

charset = u'utf-8'
version = u'1.0.3'
updateDate = u'2016-10-25'

# 选择的模版
template = "iVMS5060"
# 工程路径
projectPath = '/d/python/lang/data/'

excelpath = template+'.xlsx'

# 语言匹配的正则
rex = "\"(.*?)\"\s*=\s*\"(.*?)\""


# 当前语言索引
langs = {'zh':1,'en':2}
# 当前语言名称
langsName = {langs['zh']:'中文',langs['en']:'英文'}

# 要操作的语言资源文件
lists = [
{'name':'iVMS5060','path':{
    langs['zh']:'iVMS5060/iVMS5060/zh-Hans.lproj/Localizable.strings',
    langs['en']:'iVMS5060/iVMS5060/en.lproj/Localizable.strings'}
},
{'name':'VideoPlugin','path':{
    langs['zh']:'VideoPlayPlugin/VideoPlayPlugin/zh-Hans.lproj/Localizable.strings',
    langs['en']:'VideoPlayPlugin/VideoPlayPlugin/en.lproj/Localizable.strings'}
},
{'name':'FileManagerPlugin','path':{
    langs['zh']:'FileManagerPlugin/FileManagerPlugin/Supporting Files/zh-Hans.lproj/Localizable.strings',
    langs['en']:'FileManagerPlugin/FileManagerPlugin/Supporting Files/en.lproj/Localizable.strings'}
},
{'name':'HKGXToolbox','path':{
    langs['zh']:'HKGXToolbox/HKGXToolbox/zh-Hans.lproj/Localizable.strings',
    langs['en']:'HKGXToolbox/HKGXToolbox/en.lproj/Localizable.strings'}
},
{'name':'MapPlugin','path':{
    langs['zh']:'MapPlugin/MapPlugin/zh-Hans.lproj/Localizable.strings',
    langs['en']:'MapPlugin/MapPlugin/en.lproj/Localizable.strings'}
},
{'name':'MessagePlugin','path':{
    langs['zh']:'MessagePlugin/MessagePlugin/zh-Hans.lproj/Localizable.strings',
    langs['en']:'MessagePlugin/MessagePlugin/en.lproj/Localizable.strings'}
}
]


def linesForFile(path):
    '''
    返回指定文件的所有行
    '''
    fp = open(path,'r')
    lines = fp.readlines()
    fp.close()
    return lines

def linesByModuleName(moduleName,lang):
    '''
    根据模块和语言类型，返回文件的所有行
    '''
    for row in lists:
        if row['name'] == moduleName:
            paths = row['path']
            f = paths[lang]
            return linesForFile(f)

def decodeDataByLine(line):
    '''
    解析一条语言，如果成功，则返回这条语言key,value
    如果失败，说明不是真实的语言
    '''
    result = re.search(rex,line)
    # 找了一条语言
    if result != None:
        key = result.group(1)
        value = result.group(2)
        return (key,value)
    return None

def valueByKey(lines, key):
    '''根据key获取值
    lines 文件所有行
    key 要查找的key
    如果没有找到，则返回None
    '''
    for line in lines:
        #print line
        result = decodeDataByLine(line)
        #print line,result
        # 找了一条语言
        if result != None:
            k = result[0]
            value = result[1]
            if k == key:
                return value
    return None

def valuesExcelByKey(table, key,lang):
    '''
    指定key返回Excel语言的值
    '''
    rows = len(table.rows)
    for row in range(2,len(table.rows)+1):
        k = table.cell(row=row, column=1).value
        if k == key:
            # 第一列是key，所以这里要加1 
            value = table.cell(row=row,column=lang+1).value
            if not value:
                value = "None"
            return value
    return None

def rowExcelByKey(table,key):
    '''
    指定key返回Excel所在的位置
    '''
    # 第一行为标题，自动忽略
    for i in range(2, len(table.rows)+1):
        k = table.cell(row=i,column=1).value
        if key == k:
            return i
    return None

def excelSyncDelete(excel):
    '''
    excel同步删除语言资源不存在的键值
    '''
    print(u'正在同步已经删除语言资源 ...')
    for sheet in excel:
        # 获取中文所有行，比对key
        lines = linesByModuleName(sheet.title,langs['zh'])
        # 读取行失败了，则直接删除sheet
        if not lines:
            print(u'Excel表 %s 在本地模块中不存在，已删除'%sheet.title)
            excel.remove_sheet(sheet)
            continue
        rows = sheet.rows
        num = 0
        i = 0
        for row in rows:
            i = i + 1
            if len(row)<1:
                continue
            cell = row[0]
            k = cell.value
            if not k:
                continue
            # 第一行，跳过
            if k == "key":
                continue
            if not valueByKey(lines,k):
                num = num + 1
                for cell in row:
                    cell.value = None
        print(u'模块 %s 操作完成，发现 %d 条记录，删除 %d 条记录'%(sheet.title,len(rows),num))

    excel.save(excelpath)

def sync(excel):
    '''
    同步中文的key到excel中
    '''
    print(u'\n正在同步新增加的语言资源 ...')
    for list in lists:
        moduleName = list['name']
        path = list['path']
        f = path[langs['zh']]
        lines = linesForFile(f)
        table = None
        # table不存在，则要创建
        try:
            table = excel.get_sheet_by_name(moduleName)
        except Exception, e:
            # 首先获取一下当前的sheet，如果当前sheet数据不为空，则重新创建一个sheet
            table = excel.active
            if len(table.rows)>0:
                table = excel.create_sheet()
            table.title = moduleName
            table.cell(row=1,column=1).value = u'key'
            table.cell(row=1,column=langs['zh']+1).value = u'中文'
            table.cell(row=1,column=langs['en']+1).value = u'英文'
            print(u'创建新模块 %s 成功'%moduleName)
        total = 0
        num = 0
        for line in lines:
            result = decodeDataByLine(line)
            if result:
                total = total + 1
                k = result[0]
                v = result[1]
                if not rowExcelByKey(table,k):
                    table.append([k,v])
                    num = num + 1
        print(u'模块 %s 操作完成，发现 %d 条语言资源，共导入 %d 条新语言'%(moduleName,total,num))
    excel.save(excelpath)

def syncLang(excel,lang):
    '''
    同步英文的key到excel中
    '''
    print(u'\n正在同步 %s 语言资源 ...'%langsName[lang])
    zhPattern = re.compile(u'[\u4e00-\u9fa5]+')
    for list in lists:
        moduleName = list['name']
        path = list['path']
        f = path[lang]
        lines = linesForFile(f)
        table = excel.get_sheet_by_name(moduleName)

        total = 0
        num = 0
        confirm = True
        for line in lines:
            result = decodeDataByLine(line)
            if result:
                total = total + 1
                k = result[0]
                v = result[1]
                # 当前语言有两个及以上的汉字，则直接忽略
                zhPattern = re.compile(u'[\u4e00-\u9fa5]{2,}')
                result = zhPattern.search(v)
                if result:
                    v = ""
                row = rowExcelByKey(table,k)
                if not row:
                    print(u'错误，模块 %s ，语言 %s 没有找到，请确认已经在中文语言资源中已经增加！'%(moduleName,k))
                    continue
                value = table.cell(row=row,column=lang+1).value
                if len(v)>0:
                    if not value or len(value) < 1:
                        num = num + 1
                        table.cell(row=row, column=lang+1).value = v
                    elif value != v:
                        r = ""
                        while r <> 'y' and r <> 'n' and r <> 'yy' and confirm:
                            r = raw_input('发现冲突，模块:%s,key:%s,本地：%s,excel:%s,使用本地替换excel吗？(y/n)'%(moduleName,k,v,value))
                            r = r.lower()
                        if r == 'y' or r == 'yy' or not confirm:
                            if r == 'yy':
                                confirm = False
                            num = num + 1
                            table.cell(row=row,column=lang+1).value = v

        print(u'模块 %s 操作完成，发现 %d 条语言资源，共导入 %d 条新语言'%(moduleName,total,num))
    excel.save(excelpath)

def export(excel,lang):
    '''
    从excel生成语言文件
    '''
    print(u'\n正在从excel生成 %s 语言文件 ...'%langsName[lang])
    for list in lists:
        moduleName = list['name']
        path = list['path']
        fromPath = path[langs['zh']]
        toPath = path[lang]
        lines = linesForFile(fromPath)
        fp = open(toPath,'w')
        table = excel.get_sheet_by_name(moduleName)

        total = 0
        num = 0
        for line in lines:
            result = decodeDataByLine(line)
            if not result:
                fp.write(line)
                continue

            total = total + 1
            k = result[0]
            v = result[1]
            row = rowExcelByKey(table,k)
            if not row:
                print(u'错误，模块 %s ，语言 %s 没有找到，请确认已经在中文语言资源中已经增加！'%(moduleName,k))
                continue
            value = table.cell(row=row,column=lang+1).value
            if not value:
                value = v
            s = '"%s" = "%s";\n'%(k,value)
            fp.write(s)
            num = num + 1
        fp.close()
        print(u'模块 %s 操作完成，发现 %d 条语言资源，共生成 %d 条新语言'%(moduleName,total,num))

def reload_sys():
    if platform.system() == u'Windows':
        sys.setdefaultencoding(u'gb2312')
        reload(sys)

def main():
    global rexs,repath
    global excel
    print u'---------------------------\n语言自动同步Excel小工具 v%s(%s)\n---------------------------\n' % (version, updateDate)

    reload_sys()
    # rexs = jsonByFile(repath)
    # if not rexs:
    #     print u'读取正则模版失败'
    #     return
    #
    # path = templateDir+template+templateExt
    # templateBody = jsonByFile(path)
    # if not templateBody:
    #     print u'读取配置模版失败,path:%s' % path
    # print templateBody

    excel = libs.Excel(excelpath)
    nums = excel.sheets_names()
    print nums


    # #xls = xlrd.open_workbook(path)
    # excel = None
    # if os.path.exists(excelpath):
    #     excel = openpyxl.load_workbook(excelpath)
    # else:
    #     excel = openpyxl.Workbook()
    # # Excel文件名称
    #
    #
    #
    #
    #
    # # 同步删除
    # excelSyncDelete(excel)
    # # 同步新增加的语言
    # sync(excel)
    # # 同步已经存在的英文
    # syncLang(excel,langs['en'])
    # # 生成英文文件
    # export(excel,langs['en'])
    # print(u'\n---- \n所有操作已经完成，Excel文件名：%s' % excelpath)

if __name__ == '__main__':
    main()
