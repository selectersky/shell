# ! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-
#date 16-5-23
"""
功能：将excel文件的英文导入到工程目录中

1.0.0 2016.06.02 第一个版本完成 

"""

import openpyxl
import os,platform
import sys
import time
reload(sys)
charset = u'utf-8'
version = u'1.0.0'
updateDate = u'2016-06-02'

if platform.system() == u'Windows':
    charset = u'gb2312'
sys.setdefaultencoding(charset)

data = [
{'name':u'File_Plugin','path':u'File_Plugin/app/src/main/res/values_en/','file':'strings.xml'},
{'name':u'Video_Plugin','path':u'Video_Plugin/app/src/main/res/values_en/','file':'strings.xml'}
]
excelpath = 'android_lang.xlsx'

def main():
    print u'---------------------------\nExcel导入到语言小工具 v%s(%s)\n---------------------------\n' % (version, updateDate)
    excel = openpyxl.load_workbook(excelpath)

    table = None
    for item in data:
        #sheet不存在
        try:
            table = excel.get_sheet_by_name(item['name'])
        except Exception, e:
            print u'模块 %s 语言不存在，请先用exportLang.py工具导出到语言excel'%item['name']
            continue
        body = '''<!--  \nstrings.xml
  %s

  Created by importLangForAndroid.py on %s
  Copyright © 2016年 hikvision. All rights reserved.
-->\n<?xml version="1.0" encoding="utf-8"?>\n<resources>'''%(item['name'],time.strftime('%Y-%m-%d %H:%M:%S'))
        rows = len(table.rows)
        for row in range(2,len(table.rows)+1):
            key = table.cell(row=row, column=1).value
            chs = table.cell(row=row,column=2).value
            en = table.cell(row=row,column=3).value
            if not en:
                en = chs
            body = body + '\n    <string name="%s">%s</string>'%(key, en)
        body = body + '\n</resources>'
        path = item['path']
        if not os.path.exists(path):
            os.mkdir(path)
        fullpath = path + item['file']
        fp = open(fullpath,'w')
        fp.write(body)
        fp.close()
        print(u'操作模块 %s 完成，共生成 %d 条语言资源'%(item["name"],rows))

if __name__ == '__main__':
    main()