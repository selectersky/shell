# ! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-
#date 16-5-24

import openpyxl
import re
import os,platform
import sys
reload(sys)
charset = u'utf-8'
version = u'1.0.1'
updateDate = u'2016-05-24'

if platform.system() == u'Windows':
    charset = u'gb2312'
sys.setdefaultencoding(charset)

data = [
{'name':u'File_Plugin','path':u'File_Plugin/app/src/main/res/values/','files':['photo_strings.xml','strings.xml','video_strings.xml']},
{'name':u'Video_Plugin','path':u'Video_Plugin/app/src/main/res/values/','files':['photo_strings.xml','strings.xml','video_strings.xml']}
]
rex = '\"(.*?)\"\s*\>(.*?)\<'
excelpath = 'android_lang.xlsx'

def existsRecord(key,table,count):
    # 第一行为标题，自动忽略
    for i in range(2, count+1):
        k = table.cell(row=i,column=1).value
        if key == k:
            return True
    return False

def main():
    print u'---------------------------\n语言自动导出Excel小工具 For Android  v%s(%s)\n---------------------------\n' % (version, updateDate)
    #xls = xlrd.open_workbook(path)
    excel = None
    if os.path.exists(excelpath):
        excel = openpyxl.load_workbook(excelpath)
    else:
        excel = openpyxl.Workbook()

    table = None
    for item in data:
        #sheet不存在，则需要再创建一个
        try:
            table = excel.get_sheet_by_name(item['name'])
        except Exception, e:
            table = excel.create_sheet()
            table.title = item['name']
            table.cell(row=1,column=1).value = u'key'
            table.cell(row=1,column=2).value = u'中文'
            table.cell(row=1,column=3).value = u'英文'
        
        tableRowCount = len(table.rows)
        # print tableRowCount,table.cell(row=tableRowCount,column=1).value

        # return
        path = item['path']
        files = item['files']
        
        for f in files:
            fullpath = path + f
            fp = open(fullpath,'r')

            lines = fp.readlines()
            findNum = 0
            totalNum = 0
            for line in lines:
                #print line
                result = re.search(rex,line)
                # 找了一条语言
                if result != None:
                    totalNum = totalNum + 1
                    key = result.group(1)
                    name = result.group(2)
                    if not existsRecord(key,table,tableRowCount):
                        findNum = findNum + 1
                        row = tableRowCount + findNum
                        table.cell(row=row,column=1).value = key.decode('utf8')
                        table.cell(row=row,column=2).value = name.decode('utf8')
            fp.close()
            print(u'模块 %s，文件名:%s, 共发现 %d 条语言，导出 %d 条'%(item["name"], f, totalNum, findNum))
        print(u'操作模块 %s 完成，共 %d 个文件'%(item["name"], len(files)))
    
    excel.save(excelpath)
    print(u'所有操作已经完成，请查看当前目录 %s' % excelpath)
    raw_input()

if __name__ == '__main__':
    main()

