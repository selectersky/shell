#! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-

import sys,os
#工sf
premissions = {
'gps':[
    'ACCESS_COARSE_LOCATION',
    'ACCESS_LOCATION_EXTRA_COMMANDS',
    'ACCESS_MOCK_LOCATION',
    'INSTALL_LOCATION_PROVIDER'
    ],
'sms':[
    'BROADCAST_SMS',
    'CALL_PHONE',
    'CALL_PRIVILEGED',
    'PROCESS_OUTGOING_CALLS',
    'READ_CALL_LOG',
    'REBOOT',
    'RECEIVE_BOOT_COMPLETED',
    'RECEIVE_MMS',
    'RECEIVE_SMS',
    'RECEIVE_WAP_PUSH',
    'ADD_VOICEMAIL',
    'SEND_SMS',
    'WAKE_LOCK', 
    'WRITE_SMS'
    ],
'media':[
    'CAMERA',
    'CAPTURE_AUDIO_OUTPUT',
    'CAPTURE_SECURE_VIDEO_OUTPUT',
    'CAPTURE_VIDEO_OUTPUT',
    'RECORD_AUDIO'
    ],
'network':[
#    'ACCESS_NETWORK_STATE',
#    'ACCESS_WIFI_STATE',
    'BLUETOOTH',
    'BLUETOOTH_ADMIN',
    'BLUETOOTH_PRIVILEGED',
    'BROADCAST_WAP_PUSH',
    'CHANGE_NETWORK_STATE',
    'CHANGE_WIFI_MULTICAST_STATE',
    'CHANGE_WIFI_STATE',
    'INTERNET'
    ]
}
prefix = "android.permission."
#执行替换
#arags array 要替换的键值
#filename string 操作的文件名
def execFliter(argvs,filename):
    
    if not os.path.exists(filename):
        print "filename:",filename," not exists"
        return;
    fp = None
    try:
        fp = open(filename,"r")
    except ValueError:
        print "filename:",filename," not writeable"
        fp.close()
        return;
    body = fp.read()
    isOper = False
    for k in argvs:
        if premissions.has_key(k):
            value = premissions[k]
            for vv in value:
                s = prefix + vv
                body = body.replace(s, "")
                isOper = True
    if isOper:
        fp.close()
        try:
            fp = open(filename,"w")
            fp.write(body)
            fp.close()
        except ValueError:
            print "write data failure:",filename
    #print "body:",body
def execute(cmd):
    print 'exceute command:',cmd
    result = os.popen(cmd).read()
    print result
    tmp = result.lower()
    if tmp.find('error')!=-1 or tmp.find('fail')!=-1 or tmp.find('exception') != -1:
        print 'error'
        return False
    else:
        return True
def main():
    argv = sys.argv
    if len(argv) < 2:
        print "pls enter file name,eg:installapk.py abc.apk"
        exit(1)
    filename = argv[1]
    if not os.path.exists(filename):
        print "filename:",filename," not exists"
        exit(1);
    tmp = filename.lower();
    if tmp[-3:] != 'apk':
        print "filename:",filename," not apk file"
        exit(1)
    cmd = 'cd '+os.getcwd()+'|export PATH=${PATH}:'+os.getcwd()+'|rm -rf tmp/'
    print 'exceute command:',cmd
    os.popen(cmd)
    cmd = 'java -jar apktool.jar d -f '+filename+' tmp'
    if execute(cmd) == False:
        print "unpack ",filename,' error'
        exit(1)
    xmlfilename = 'tmp/AndroidManifest.xml'
    if len(argv) > 2:
        print 'use user config:',argv[1:]
        execFliter(argv[1:],xmlfilename)
    else:
        print 'use default config:[network,sms]'
        #execFliter(['network','sms'],xmlfilename)
    cmd = 'java -jar apktool.jar b -f -a '+os.getcwd()+'/aapt tmp/ tmp.apk'
    if execute(cmd) == False:
        print "pack ",filename,' error'
        exit(1)
    cmd = 'java -jar signapk.jar testkey.x509.pem testkey.pk8 tmp.apk tmp_signed.apk'
    if execute(cmd) == False:
        print 'sign apk error'
        exit(1)
    cmd = os.getcwd()+'/adb install tmp_signed.apk'
    if execute(cmd) == False:
        print 'install apk failue'
        exit(1)
    cmd = 'rm tmp.apk tmp_signed.apk'
    os.system(cmd)
    
main()