# mdb2mysql
## 依赖组件：
`pywin32-219.win-amd64-py2.7.exe` 下载地址：<http://sourceforge.net/projects/pywin32/>

## XP下md连接串用Microsoft.Jet.OLEDB.4.0，windows 7下连接串用Microsoft.ACE.OLEDB.12.0
需要下载：access database engine 下载地址：
<http://www.microsoft.com/zh-CN/download/details.aspx?id=13255>

## mysql python连接器下载地址：

<http://dev.mysql.com/downloads/connector/python/>


## 手册地址：
<http://dev.mysql.com/doc/connector-python/en/connector-python-example-cursor-transaction.html>
