#! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-

import win32com.client   
import mysql.connector
import string
import HTMLParser
import re

conn = win32com.client.Dispatch(r'ADODB.Connection')   
DSN = r'PROVIDER=Microsoft.Jet.OLEDB.4.0;DATA SOURCE=D:/web/gaylkj/data/#ca12zy.mdb;'   
#DSN = r'PROVIDER=Microsoft.ACE.OLEDB.12.0;DATA SOURCE=D:/web/gaylkj/data/1.mdb;'   
conn.Open(DSN)

myconn=mysql.connector.connect(user='root', password='',database='db',host='127.0.0.1')

rs = win32com.client.Dispatch(r'ADODB.Recordset')   
html_parser = HTMLParser.HTMLParser()

def procate():
	
	cur=myconn.cursor()
	#删除中文分类
	sql = 'delete from gk_category where parentid=20'
	cur.execute(sql)
	#删除英文分类
	sql = 'delete from gk_category where parentid=21'
	cur.execute(sql)
	rs_name = 'P_class'#表名   
	rs.Cursorlocation = 3
	rs.Open('Select * FROM [' + rs_name + '] order by p_id asc', conn, 1, 1)
	print u'开始导入中文分类,共有记录 %d 条...'%rs.RecordCount
	total = 0
	ids = '20'
	while not rs.EOF:

		pid = rs.Fields.Item(0).Value
		#中文标题
		name = rs.Fields.Item(1).Value
		sql = "insert into gk_category (\
			id, \
			catname,\
			catdir,\
			parentdir,\
			parentid,\
			moduleid,\
			module,\
			arrparentid,\
			arrchildid,\
			type,\
			title,\
			keywords,\
			description,\
			listorder,\
			ishtml,\
			ismenu,\
			hits,\
			image,\
			child,\
			url,\
			template_list,\
			template_show,\
			pagesize,\
			readgroup,\
			listtype,\
			lang,\
			urlruleid,\
			presentpoint,\
			chargepoint,\
			paytype,\
			repeatchargedays,\
			postgroup)values(\
			%(id)s, \
			%(catname)s,\
			%(catdir)s,\
			%(parentdir)s,\
			%(parentid)s,\
			%(moduleid)s,\
			%(module)s,\
			%(arrparentid)s,\
			%(arrchildid)s,\
			%(type)s,\
			%(title)s,\
			%(keywords)s,\
			%(description)s,\
			%(listorder)s,\
			%(ishtml)s,\
			%(ismenu)s,\
			%(hits)s,\
			%(image)s,\
			%(child)s,\
			%(url)s,\
			%(template_list)s,\
			%(template_show)s,\
			%(pagesize)s,\
			%(readgroup)s,\
			%(listtype)s,\
			%(lang)s,\
			%(urlruleid)s,\
			%(presentpoint)s,\
			%(chargepoint)s,\
			%(paytype)s,\
			%(repeatchargedays)s,\
			%(postgroup)s)"
		data_sql = {'id':pid,
			'catname':name,
			'catdir':'cate%d'%pid,
			'parentdir':'products/',
			'parentid':20,
			'moduleid':3,
			'module':'Product',
			'arrparentid':'0,20',
			'arrchildid':pid,
			'type':0,
			'title':'',
			'keywords':'',
			'description':'',
			'listorder':0,
			'ishtml':0,
			'ismenu':1,
			'hits':0,
			'image':'',
			'child':0,
			'url':'/index.php?a=index&m=Product&id=%d'%pid,
			'template_list':'',
			'template_show':'',
			'pagesize':0,
			'readgroup':'',
			'listtype':0,
			'lang':0,
			'urlruleid':4,
			'presentpoint':0,
			'chargepoint':0,
			'paytype':0,
			'repeatchargedays':0,
			'postgroup':''}		
		
		cur.execute(sql, data_sql)		
		sql = "update gk_category set arrchildid='%s' where id=20"%ids	
		cur.execute(sql)
		
		rs.MoveNext()
		total = total + 1
		ids = '%s,%d'%(ids,pid)

	rs.Close()
	print u'导入中文分类完成，共导入 %d 条数据'%total

	rs.Cursorlocation = 3
	rs.Open('Select * FROM [' + rs_name + '] order by p_id asc', conn, 1, 1)
	print u'开始导入英文分类,共有记录 %d 条...'%rs.RecordCount
	total = 0
	ids = '21'

	while not rs.EOF:

		pid = rs.Fields.Item(0).Value
		#英文标题
		name_e = rs.Fields.Item(2).Value
		#计算规则：当前id+(导入前表的最大值ID-导入前表的最小值 ID)+1
		catid  = pid + (63-40) + 1	
		
		sql = "insert into gk_category (\
			id, \
			catname,\
			catdir,\
			parentdir,\
			parentid,\
			moduleid,\
			module,\
			arrparentid,\
			arrchildid,\
			type,\
			title,\
			keywords,\
			description,\
			listorder,\
			ishtml,\
			ismenu,\
			hits,\
			image,\
			child,\
			url,\
			template_list,\
			template_show,\
			pagesize,\
			readgroup,\
			listtype,\
			lang,\
			urlruleid,\
			presentpoint,\
			chargepoint,\
			paytype,\
			repeatchargedays,\
			postgroup)values(\
			%(id)s, \
			%(catname)s,\
			%(catdir)s,\
			%(parentdir)s,\
			%(parentid)s,\
			%(moduleid)s,\
			%(module)s,\
			%(arrparentid)s,\
			%(arrchildid)s,\
			%(type)s,\
			%(title)s,\
			%(keywords)s,\
			%(description)s,\
			%(listorder)s,\
			%(ishtml)s,\
			%(ismenu)s,\
			%(hits)s,\
			%(image)s,\
			%(child)s,\
			%(url)s,\
			%(template_list)s,\
			%(template_show)s,\
			%(pagesize)s,\
			%(readgroup)s,\
			%(listtype)s,\
			%(lang)s,\
			%(urlruleid)s,\
			%(presentpoint)s,\
			%(chargepoint)s,\
			%(paytype)s,\
			%(repeatchargedays)s,\
			%(postgroup)s)"
		data_sql = {'id':catid,
			'catname':name_e,
			'catdir':'cate%d'%catid,
			'parentdir':'products/',
			'parentid':21,
			'moduleid':3,
			'module':'Product',
			'arrparentid':'0,21',
			'arrchildid':catid,
			'type':0,
			'title':'',
			'keywords':'',
			'description':'',
			'listorder':0,
			'ishtml':0,
			'ismenu':1,
			'hits':0,
			'image':'',
			'child':0,
			'url':'/index.php?a=index&m=Product&g=English&id=%d'%catid,
			'template_list':'',
			'template_show':'',
			'pagesize':0,
			'readgroup':'',
			'listtype':0,
			'lang':1,
			'urlruleid':4,
			'presentpoint':0,
			'chargepoint':0,
			'paytype':0,
			'repeatchargedays':0,
			'postgroup':''}		
		
		cur.execute(sql, data_sql)		

		sql = "update gk_category set arrchildid='%s' where id=21"%ids	
		cur.execute(sql)
		
		rs.MoveNext()
		total = total + 1	
		ids = '%s,%d'%(ids,catid)

	rs.Cursorlocation = 3
	rs.Open('Select * FROM [' + rs_name + '] order by p_id asc', conn, 1, 1)
	print u'开始导入中文分类,共有记录 %d 条...'%rs.RecordCount
	total = 0
	ids = '20'
	while not rs.EOF:

		pid = rs.Fields.Item(0).Value
		#中文标题
		name = rs.Fields.Item(1).Value
		sql = "insert into gk_category (\
			id, \
			catname,\
			catdir,\
			parentdir,\
			parentid,\
			moduleid,\
			module,\
			arrparentid,\
			arrchildid,\
			type,\
			title,\
			keywords,\
			description,\
			listorder,\
			ishtml,\
			ismenu,\
			hits,\
			image,\
			child,\
			url,\
			template_list,\
			template_show,\
			pagesize,\
			readgroup,\
			listtype,\
			lang,\
			urlruleid,\
			presentpoint,\
			chargepoint,\
			paytype,\
			repeatchargedays,\
			postgroup)values(\
			%(id)s, \
			%(catname)s,\
			%(catdir)s,\
			%(parentdir)s,\
			%(parentid)s,\
			%(moduleid)s,\
			%(module)s,\
			%(arrparentid)s,\
			%(arrchildid)s,\
			%(type)s,\
			%(title)s,\
			%(keywords)s,\
			%(description)s,\
			%(listorder)s,\
			%(ishtml)s,\
			%(ismenu)s,\
			%(hits)s,\
			%(image)s,\
			%(child)s,\
			%(url)s,\
			%(template_list)s,\
			%(template_show)s,\
			%(pagesize)s,\
			%(readgroup)s,\
			%(listtype)s,\
			%(lang)s,\
			%(urlruleid)s,\
			%(presentpoint)s,\
			%(chargepoint)s,\
			%(paytype)s,\
			%(repeatchargedays)s,\
			%(postgroup)s)"
		data_sql = {'id':pid,
			'catname':name,
			'catdir':'cate%d'%pid,
			'parentdir':'products/',
			'parentid':20,
			'moduleid':3,
			'module':'Product',
			'arrparentid':'0,20',
			'arrchildid':pid,
			'type':0,
			'title':'',
			'keywords':'',
			'description':'',
			'listorder':0,
			'ishtml':0,
			'ismenu':1,
			'hits':0,
			'image':'',
			'child':0,
			'url':'/index.php?a=index&m=Product&id=%d'%pid,
			'template_list':'',
			'template_show':'',
			'pagesize':0,
			'readgroup':'',
			'listtype':0,
			'lang':0,
			'urlruleid':4,
			'presentpoint':0,
			'chargepoint':0,
			'paytype':0,
			'repeatchargedays':0,
			'postgroup':''}		
		
		cur.execute(sql, data_sql)		
		sql = "update gk_category set arrchildid='%s' where id=20"%ids	
		cur.execute(sql)
		
		rs.MoveNext()
		total = total + 1
		ids = '%s,%d'%(ids,pid)

	rs.Close()
	print u'导入中文分类完成，共导入 %d 条数据'%total

	rs.Cursorlocation = 3
	rs.Open('Select * FROM [' + rs_name + '] order by p_id asc', conn, 1, 1)
	print u'开始导入英文分类,共有记录 %d 条...'%rs.RecordCount
	total = 0
	ids = '21'

	while not rs.EOF:

		pid = rs.Fields.Item(0).Value
		#英文标题
		name_e = rs.Fields.Item(2).Value
		#计算规则：当前id+(导入前表的最大值ID-导入前表的最小值 ID)+1
		catid  = pid + (63-40) + 1	
		
		sql = "insert into gk_category (\
			id, \
			catname,\
			catdir,\
			parentdir,\
			parentid,\
			moduleid,\
			module,\
			arrparentid,\
			arrchildid,\
			type,\
			title,\
			keywords,\
			description,\
			listorder,\
			ishtml,\
			ismenu,\
			hits,\
			image,\
			child,\
			url,\
			template_list,\
			template_show,\
			pagesize,\
			readgroup,\
			listtype,\
			lang,\
			urlruleid,\
			presentpoint,\
			chargepoint,\
			paytype,\
			repeatchargedays,\
			postgroup)values(\
			%(id)s, \
			%(catname)s,\
			%(catdir)s,\
			%(parentdir)s,\
			%(parentid)s,\
			%(moduleid)s,\
			%(module)s,\
			%(arrparentid)s,\
			%(arrchildid)s,\
			%(type)s,\
			%(title)s,\
			%(keywords)s,\
			%(description)s,\
			%(listorder)s,\
			%(ishtml)s,\
			%(ismenu)s,\
			%(hits)s,\
			%(image)s,\
			%(child)s,\
			%(url)s,\
			%(template_list)s,\
			%(template_show)s,\
			%(pagesize)s,\
			%(readgroup)s,\
			%(listtype)s,\
			%(lang)s,\
			%(urlruleid)s,\
			%(presentpoint)s,\
			%(chargepoint)s,\
			%(paytype)s,\
			%(repeatchargedays)s,\
			%(postgroup)s)"
		data_sql = {'id':catid,
			'catname':name_e,
			'catdir':'cate%d'%catid,
			'parentdir':'products/',
			'parentid':21,
			'moduleid':3,
			'module':'Product',
			'arrparentid':'0,21',
			'arrchildid':catid,
			'type':0,
			'title':'',
			'keywords':'',
			'description':'',
			'listorder':0,
			'ishtml':0,
			'ismenu':1,
			'hits':0,
			'image':'',
			'child':0,
			'url':'/index.php?a=index&m=Product&g=English&id=%d'%catid,
			'template_list':'',
			'template_show':'',
			'pagesize':0,
			'readgroup':'',
			'listtype':0,
			'lang':1,
			'urlruleid':4,
			'presentpoint':0,
			'chargepoint':0,
			'paytype':0,
			'repeatchargedays':0,
			'postgroup':''}		
		
		cur.execute(sql, data_sql)		

		sql = "update gk_category set arrchildid='%s' where id=21"%ids	
		cur.execute(sql)
		
		rs.MoveNext()
		total = total + 1	
		ids = '%s,%d'%(ids,catid)
		
	cur.close()
	rs.Close()
	print u'导入英文分类完成，共导入 %d 条数据'%total

def news():
	cur=myconn.cursor()
	sql = 'TRUNCATE TABLE gk_article'
	cur.execute(sql)
	rs_name = 'News'#表名   
	rs.Cursorlocation = 3
	rs.Open('Select * FROM [' + rs_name + '] order by news_id asc', conn, 1, 1)
	print u'开始导入新闻,共有记录 %d 条...'%rs.RecordCount
	total = 0
	while not rs.EOF:
		pid = rs.Fields.Item(0).Value
		catid = rs.Fields.Item(1).Value		
		title = rs.Fields.Item(3).Value
		author = rs.Fields.Item(4).Value
		date = rs.Fields.Item(5).Value
		content = rs.Fields.Item(7).Value
		keywords = rs.Fields.Item(8).Value
		num = rs.Fields.Item(9).Value

		content = html_parser.unescape(content)
		#图片引起大小写统一
		result,number = re.subn(ur'/?[u|U]pload[f|F]ile','/UploadFile',content)
		content = result



		#默认是中文公司新闻
		classid = 3
		group = ''
		lang = 0
		#中文技术资料
		if catid == 11:
			classid = 17
		#英文公司新闻
		if catid == 1:
			classid = 22
			group = '&g=English'
			lang = 1
		#英文技术资料
		if catid == 12:
			classid = 23
			group = '&g=English'
			lang = 1
		
		print u'import id %d'%pid
		
		

		sql = "insert into gk_article (\
			id,\
			catid,\
			userid,\
			username,\
			title,\
			title_style,\
			keywords,\
			copyfrom,\
			fromlink,\
			description,\
			content,\
			template,\
			thumb,\
			posid,\
			status,\
			recommend,\
			readgroup,\
			readpoint,\
			listorder,\
			url,\
			hits,\
			createtime,\
			updatetime,\
			name,\
			lang\
			)values(%(id)s,\
			%(catid)s,\
			%(userid)s,\
			%(username)s,\
			%(title)s,\
			%(title_style)s,\
			%(keywords)s,\
			%(copyfrom)s,\
			%(fromlink)s,\
			%(description)s,\
			%(content)s,\
			%(template)s,\
			%(thumb)s,\
			%(posid)s,\
			%(status)s,\
			%(recommend)s,\
			%(readgroup)s,\
			%(readpoint)s,\
			%(listorder)s,\
			%(url)s,\
			%(hits)s,\
			%(createtime)s,\
			%(updatetime)s,\
			%(name)s,\
			%(lang)s)"
		data = {'id':pid,
			'catid':classid,
			'userid':1,
			'username':'gaylkj',
			'title':title,
			'title_style':'',
			'keywords':'',
			'copyfrom':'',
			'fromlink':'',
			'description':'',
			'content':content,
			'template':0,
			'thumb':'',
			'posid':0,
			'status':1,
			'recommend':0,
			'readgroup':'',
			'readpoint':0,
			'listorder':0,
			'url':'/index.php?a=show&m=Article%s&id=%d'%(group,pid),
			'hits':0,
			'createtime':1411824690,
			'updatetime':1411824690,
			'name':'',
			'lang':lang}
		cur.execute(sql,data)
		rs.MoveNext()		
		total = total + 1
	print u'导入新闻完成,共导入记录 %d 条...'%total
	rs.Close()
	cur.close()

def product():
	cur=myconn.cursor()
	#sql = 'TRUNCATE TABLE gk_product'
	#cur.execute(sql)
	rs_name = 'P_info'#表名   
	rs.Cursorlocation = 3
	rs.Open('Select * FROM [' + rs_name + '] order by p_id asc', conn, 1, 1)
	print u'开始导入产品,共有记录 %d 条...'%rs.RecordCount
	total = 0
	while not rs.EOF:
		pid = rs.Fields.Item(0).Value
		catid = rs.Fields.Item(3).Value
		title = rs.Fields.Item(5).Value
		spec = rs.Fields.Item(6).Value
		pic = rs.Fields.Item(7).Value
		content = rs.Fields.Item(11).Value
		order = rs.Fields.Item(12).Value
		title_e = rs.Fields.Item(15).Value
		content_e  = rs.Fields.Item(18).Value
		#图片引起大小写统一
		result,number = re.subn(ur'/?[u|U]pload[f|F]ile','/UploadFile',pic)
		pic = result

		content = html_parser.unescape(content)
		#图片引起大小写统一
		result,number = re.subn(ur'/?[u|U]pload[f|F]ile','/UploadFile',content)
		content = result

		content_e = html_parser.unescape(content_e)
		#图片引起大小写统一
		result,number = re.subn(ur'/?[u|U]pload[f|F]ile','/UploadFile',content_e)
		content_e = result

		print u'导入记录%d'%pid
		#插入中文
		sql = "insert into gk_product (id,\
			catid,\
			userid,\
			username,\
			title,\
			title_style,\
			keywords,\
			description,\
			content,\
			template,\
			thumb,\
			posid,\
			status,\
			recommend,\
			readgroup,\
			readpoint,\
			listorder,\
			hits,\
			createtime,\
			updatetime,\
			price,\
			url,\
			xinghao,\
			pics,\
			language\
			)values(%(pid)s,\
			%(catid)s,\
			%(userid)s,\
			%(username)s,\
			%(title)s,\
			%(title_style)s,\
			%(keywords)s,\
			%(description)s,\
			%(content)s,\
			%(template)s,\
			%(thumb)s,\
			%(posid)s,\
			%(status)s,\
			%(recommend)s,\
			%(readgroup)s,\
			%(readpoint)s,\
			%(listorder)s,\
			%(hits)s,\
			%(createtime)s,\
			%(updatetime)s,\
			%(price)s,\
			%(url)s,\
			%(xinghao)s,\
			%(pics)s,\
			%(language)s)"
		data = {
			'pid':pid,
			'catid':catid,
			'userid':1,
			'username':'gaylkj',
			'title':title,
			'title_style':'',
			'keywords':'',
			'description':'',
			'content':content,
			'template':0,
			'thumb':pic,
			'posid':1,
			'status':1,
			'recommend':0,
			'readgroup':'',
			'readpoint':0,
			'listorder':0,
			'hits':0,
			'createtime':'1411822700',
			'updatetime':'1411822700',
			'price':'0.00',
			'url':'/index.php?a=show&m=Product&id=%d'%pid,
			'xinghao':'',
			'pics':'',
			'language':0,}
		cur.execute(sql,data)
		
		rs.MoveNext()		
		total = total + 1
	print u'导入中文产品完成，共导入%d条'%total
	rs.Close()

	rs.Cursorlocation = 3
	rs.Open('Select * FROM [' + rs_name + '] order by p_id asc', conn, 1, 1)
	print u'开始导入英文产品,共有记录 %d 条...'%rs.RecordCount
	total = 0
	while not rs.EOF:
		pid = rs.Fields.Item(0).Value
		catid = rs.Fields.Item(3).Value
		title = rs.Fields.Item(5).Value
		spec = rs.Fields.Item(6).Value
		pic = rs.Fields.Item(7).Value
		content = rs.Fields.Item(11).Value
		order = rs.Fields.Item(12).Value
		title_e = rs.Fields.Item(15).Value
		content_e  = rs.Fields.Item(18).Value
		#图片引起大小写统一
		result,number = re.subn(ur'/?[u|U]pload[f|F]ile','/UploadFile',pic)
		pic = result

		content = html_parser.unescape(content)
		#图片引起大小写统一
		result,number = re.subn(ur'/?[u|U]pload[f|F]ile','/UploadFile',content)
		content = result

		content_e = html_parser.unescape(content_e)
		#图片引起大小写统一
		result,number = re.subn(ur'/?[u|U]pload[f|F]ile','/UploadFile',content_e)
		content_e = result

		print u'导入记录%d'%pid		

		#插入英文
		##计算分类规则：当前分类id+(导入前表分类的最大值ID-导入前表分类的最小值 ID)+1
		catid_e  = catid + (63-40) + 1	
		#计算ID规则：当前ID+(导入前表最大id-导入前最小ID)+1
		pid_e = pid + (1826-1471) + 1
		sql = "insert into gk_product (id,\
			catid,\
			userid,\
			username,\
			title,\
			title_style,\
			keywords,\
			description,\
			content,\
			template,\
			thumb,\
			posid,\
			status,\
			recommend,\
			readgroup,\
			readpoint,\
			listorder,\
			hits,\
			createtime,\
			updatetime,\
			price,\
			url,\
			xinghao,\
			pics,\
			language\
			)values(\
			%(pid)s,\
			%(catid)s,\
			%(userid)s,\
			%(username)s,\
			%(title)s,\
			%(title_style)s,\
			%(keywords)s,\
			%(description)s,\
			%(content)s,\
			%(template)s,\
			%(thumb)s,\
			%(posid)s,\
			%(status)s,\
			%(recommend)s,\
			%(readgroup)s,\
			%(readpoint)s,\
			%(listorder)s,\
			%(hits)s,\
			%(createtime)s,\
			%(updatetime)s,\
			%(price)s,\
			%(url)s,\
			%(xinghao)s,\
			%(pics)s,\
			%(language)s)"
		data = {
			'pid':pid_e,
			'catid':catid_e,
			'userid':1,
			'username':'gaylkj',
			'title':title_e,
			'title_style':'',
			'keywords':'',
			'description':'',
			'content':content_e,
			'template':0,
			'thumb':pic,
			'posid':1,
			'status':1,
			'recommend':0,
			'readgroup':'',
			'readpoint':0,
			'listorder':0,
			'hits':0,
			'createtime':'1411822700',
			'updatetime':'1411822700',
			'price':'0.00',
			'url':'/index.php?a=show&m=Product&g=English&id=%d'%pid_e,
			'xinghao':'',
			'pics':'',
			'language':1}
		cur.execute(sql,data)

		rs.MoveNext()		
		total = total + 1
	print u'导入英文产品完成,共导入记录 %d 条...'%total
	cur.close()
	rs.Close()

def main():
	#procate()
	#news()
	product()
	

if __name__ == '__main__':
    main()

