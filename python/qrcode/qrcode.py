#! /usr/bin/env python
# coding=utf-8
# -*- coding: utf-8 -*-
# date 2017/4/24
# author chenjiaxin
# version:1.0

"""
功能：

"""
import sys,os

import urllib,urllib2

# decode qrcode
from PIL import Image
import zbarlight

# encode qrcode
import pyqrcode

def download(url):
    response = urllib2.urlopen(url) 
    data = response.read()

    path = '/tmp/tmp.png'
    file=open(path, "wb")
    file.write(data)
    file.flush()
    file.close()
    #data = urllib.urlretrieve(url,path) 
    return path

def qrcode_encode(text, path='/tmp/tmp.png'):
    qr = pyqrcode.create(text)
    qr.png(path, scale=11)
    with open(path, 'rb') as image_file:
       image = Image.open(image_file)
       image.load()
       image.show()

def qrcode_decode(path):
    with open(path, 'rb') as image_file:
        image = Image.open(image_file)
        image.load()

    codes = zbarlight.scan_codes('qrcode', image)
    if len(codes)<0:
    	print 'decode failure:%s'%path
    	return ''
    r = codes[0]
    return r

def main():
    if len(sys.argv) != 3:
        print 'e.g.:%s <d|e> <file|text>'%os.path.basename(__file__)
        exit(1)

    m = sys.argv[1]
    t = sys.argv[2]

    if len(m) < 1 or len(t) < 1:
        print 'para error(%s,%s)'%(m,t)
        exit(1)

    if m == 'd' or m == 'decode':
    	tt = t
    	if t[:4] == 'http':
    		tt = download(t)
   
        if not os.path.exists(tt):
            print 'path:%s not exist'%t
            exit(1)
        print qrcode_decode(tt)
    elif m == 'e' or m == 'encode':
        qrcode_encode(t)
    else:
    	exit(1)
    exit(0)


if __name__ == '__main__':
    main()