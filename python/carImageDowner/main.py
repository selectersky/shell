#! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-

"""
功能：


"""

import json
import os
from Cjx.http import httpCient

__author__ = 'selectersky'


URL = ""

http = None
json_name = 'data.json'
progress_name = 'p.txt'


def sohu():
    '''
    将cate.txt的目录内容转换为json格式，并写入data.json
    json格式：{"车名":[{"name":"车型","url":"图片列表地址"},{"name":"车型","url":"图片列表地址"}]}  
    不同网站的数据可以导成这个json格式，可以通用处理
    '''
    #url = 'http://db.auto.sohu.com/photo/'
    #content = http.urlcontent(url)
    filename = 'cate.txt'
    fp = open(filename)
    content = fp.read()
    fp.close()

    bcatelist = http.itemlist(content,'<h4\ class=\"brand_tit\"\ >(.*?)</h4>')
    sbcateclist = http.itemlist(content,'<ul\ class=\"tree_con\">(.*?)</h4>')
    #print sbcateclist
    i = 0
    json = '';
    for row in sbcateclist['list']:
        bcatname = http.trimHtmlChar(bcatelist['list'][i])
        if json == '':
            json = '{"'+bcatname+'":['
        else:
            json = json + ',"'+bcatname+'":['

        model_list = http.itemlist(row,'<li>(.*?)</li>')
        j = 0
        for row2 in model_list['list']:
            item = http.itemDetail(row2,{'id':'model_(.*?)/picture\.shtml','name':'</em>(.*?)<span>'})
            if j == 0:
                url  = 'http://db.auto.sohu.com/model_%s/photo_t1000.shtml'%item['id']
                json = json + '{"name":"'+item['name']+'","url":"'+url+'"}'
            else:
                json = json + ',{"name":"'+item['name']+'","url":"'+url+'"}'
            j = j + 1
        json = json + ']'

        i = i + 1

    json = json + '}'
    #print bcatelist
    f = open(json_name,'w')
    f.write(json)
    f.close()


def cateitem(data,key,item_start_pos=0,image_start_pos=0):
    '''
    处理分类里面的一条数据

    data json数据 需要上面指定的数据格式
    key 要读取的哪个值
    key_pos 当前键所在的索引 用于保存进度
    item_start_pos 指定item列表开始索引
    image_start_pos 指定item的图片列表中开始保存图片的索引

    '''
    if not os.path.exists(key):
        os.mkdir(key)
    item = data[key]
    print u'处理%s,当前共有%d条数据'%(key,len(item))
    for item_pos, row in enumerate(item):

        #跳过已经保存的值 
        if item_pos<item_start_pos:
            print u'跳过条目:%d,历史索引:%d'%(item_pos,item_start_pos)
            continue
        path = key + '\\' + row['name']
        if not os.path.exists(path):
            os.mkdir(path)
        #print u'k:%s,path:%s,item_pos:%d,item_start_pos:%d,image_start_pos:%d'%(key,path,item_pos,item_start_pos,image_start_pos)
        if item_pos == item_start_pos:
            save(key,row['url'], path, item_pos,image_start_pos)
        else:
            save(key,row['url'], path, item_pos,0)


def save(key,url,savepath,item_pos,image_start_pos):
    '''
    保存指定网址的图片列表

    url 图片列表网址
    savepath 保存路径 这里没有文件名
    item_pos 当前数组的索引 用于记录当前的进度
    '''
    # fp = open(filename)
    # data = fp.read()
    # fp.close()
    # #data = '{"AC Schnitzer(497)":[{"name":"ACS5","url":"http://db.auto.sohu.com/model_3517/photo_t1000.shtml"},{"name":"ACS6","url":"http://db.auto.sohu.com/model_3517/photo_t1000.shtml"},{"name":"ACS7","url":"http://db.auto.sohu.com/model_3517/photo_t1000.shtml"}]}'
    # json_obj = json.loads(data)
    # for i in json_obj:

    #url = 'http://db.auto.sohu.com/model_3517/photo_t1000.shtml'
    content = http.urlcontent(url)
    #print content
    #exit()
    if not content:

        print u'获取内容失败，请确认网络正常且地址正确;url:%s,pos:%d'%(url,item_pos)
        return
    list = http.itemlist(content,'\"\ src=\"http://m(.*?)\"></a></span><p><a')
    count = len(list['list'])
    if count == 0:
        print u'没有获取到任何图片信息，url:%s,pos:%d'%(url,item_pos)
        return
    succ = 0
    for image_pos, item in enumerate(list['list']):
        if image_pos<image_start_pos:
            print u'跳过图片索引%d,历史索引:%d'%(image_pos,image_start_pos)
            continue
        if not item:
            print u'图片地址不正确，跳过,url:%s,image_pos:%d'%(item,image_pos)
            continue
        url = 'http://m%s'%item
        #小图换成大图
        url = url.replace('150','1200')
        #print url
        arr = url.split('/')
        name = arr[len(arr)-1]
        path = savepath + '//' + name
        try:
            body = http.download(url,path)
            succ = succ + 1
            #写进度
            fp = open(progress_name,'w')
            s = u'%s,%d,%d'%(key,item_pos,image_pos)
            fp.write(s)
            fp.close()
            p = float(image_pos) / float(count)

            print u'处理%s,下载图片成功，完成%.2f%%'%(key,p)
        except Exception, e:            
            print u'下载图片失败,url:%s,image_pos:%d,name:%s'%(url,image_pos,name)
    print u'操作%s完成，共找到%d个图片，成功下载%d个图片'%(key,count,succ)



def main():

    global http
    http = httpCient()
    fp = open(json_name,'r')
    body = fp.read()
    fp.close()

    data = json.loads(body)
    start_key = None
    start_pos = 0
    start_image_pos = 0

    if os.path.exists(progress_name):
        fp = open(progress_name,'r')
        body = fp.read()
        arr = body.split(',')
        if len(arr) != 3:
            print u'读取进度失败，请确认进度文件的格式是否正确'
            return
        start_key = arr[0]
        start_pos = int(arr[1])
        start_image_pos = int(arr[2])
    #cate()
    isFirst = True
    for key in data:
        if start_key:
            if key != start_key and isFirst:
                isFirst = False
                continue
        cateitem(data,key,start_pos,start_image_pos)

if __name__ == '__main__':
    main()

