#! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-

"""
功能：
登录 house.cnyw.net，进入发布的房产列表，读取已经发布的房产列表ID，保存到本地的result.txt文件中
"""


from Cjx.http import httpCient

import string
import Image,cStringIO, urllib2

__author__ = 'selectersky'


URL = ""

http = None

pagecount = 7

def showcode():
    url = 'http://house.cnyw.net/include/seccode.php'
    file = urllib2.urlopen(url)
    tmpIm = cStringIO.StringIO(file.read())
    image = Image.open(tmpIm)
    image.show()

def login(u, p):   

    url = 'http://house.cnyw.net/login_logout.php'
    content = http.urlcontent(url)
    
    showcode()
    code = raw_input(u'请输入你验证码：')
    print u'正在登录%s(%s) ...' %(u,p)
    url = 'http://house.cnyw.net/login_logout.php?action=login'
    param = {u'username':u,u'password':p,u'imgcode':code}
    content = http.urlcontent(url,param)
    if content.find('错误') != -1:
        print content
        return False

    return True
    #url = ''
    #print u'code'

def listid(page):
    print u'获取第%d页数据 ...'%page
    url = 'http://house.cnyw.net/user.php?ac=house_main&action=sell&garden=&page=%d'%page
    re = 'value="(\d{1,10})"'
    content = http.urlcontent(url)
    list = http.categroyList(content,re)
    print u'成功，第%d页共获取到%d条数据'%(page,len(list))
    return list

def main():

    global http
    http = httpCient()
    f = open('result.txt', "w")

    if login('username','passwd'):
        page = pagecount
        while page>0:
            list = listid(page)
            f.writelines(x+"\n" for x in list)
            page = page - 1

    print u'所有操作已经完成，结果保存在当前目录result.txt文件中'
    f.close()
        #listid(7)
    #thread.start_new_thread(showcode, (1,1))
    

if __name__ == '__main__':
    main()

