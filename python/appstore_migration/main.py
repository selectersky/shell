#! /usr/bin/env python
# coding=utf-8
# -*- coding: utf-8 -*-
# date 2017/4/14
# author chenjiaxin
# version:1.0

"""
功能：
1、得到最新的App
	SELECT * FROM tbl_apps WHERE tbl_apps.id IN ( SELECT max(id) FROM tbl_apps GROUP BY packagename,platform)
2、得到最新的Plugin
   select * from tbl_plugins where id in (select max(id) from tbl_plugins where appid =#{appid} group by packageName);

"""
import MySQLdb
import os
import random
import shutil

# decode qrcode
from PIL import Image
import zbarlight

# encode qrcode
import pyqrcode

source_path = '/Volumes/'
dest_path = '/d/web/Uploads/'
apps_new = 'tbl_apps_new'
apps_history = 'tbl_apps_hisory'
plug_new = 'tbl_plugins_new'
conn = MySQLdb.connect(host='localhost', user='root', passwd='root', port=3306)
conn.select_db('appstore')
# template = None
argv[0]

def init():
    global qrcode_reader
    # global template
    if not os.path.exists(dest_path):
        os.mkdir(dest_path)

    cur = conn.cursor()

    # 应用表
    sql = 'DROP TABLE IF EXISTS %s'%apps_new
    cur.execute(sql)
    sql = 'CREATE TABLE %s SELECT * FROM tbl_apps WHERE tbl_apps.id IN ( SELECT max(id) FROM tbl_apps GROUP BY packagename,platform)'%apps_new
    cur.execute(sql)

    # 新建字段 shorturl 短链接,updatedesc 更新描述,old_record 是否老记录（0不是 1是 2是，且记录存在异常）,old_down_url 老记录二维码下载地址
    sql = "ALTER TABLE `%s` ADD `shorturl` VARCHAR(30) NOT NULL DEFAULT '' AFTER `filemd5`, ADD `updatedesc` " \
            "VARCHAR(300) NOT NULL DEFAULT '' AFTER `shorturl`, ADD `old_record` INT NOT NULL DEFAULT 0 AFTER `updatedesc`," \
            "ADD `old_down_url` VARCHAR(150) NOT NULL DEFAULT '' AFTER `old_record`;"%apps_new
    cur.execute(sql)

    # 应用更新历史表
    sql = 'DROP TABLE IF EXISTS %s'%apps_history
    cur.execute(sql)
    sql = "CREATE TABLE `%s` ( `id` INT NOT NULL AUTO_INCREMENT , `appid` INT NOT NULL , `updatedesc` VARCHAR(300) NOT NULL , `updatetime` INT NOT NULL, PRIMARY KEY (`id`) ) ENGINE = InnoDB;"%apps_history
    cur.execute(sql)

    # 插件表
    sql = 'DROP TABLE IF EXISTS %s'%plug_new
    cur.execute(sql)
    sql = 'CREATE TABLE %s like tbl_plugins'%plug_new
    cur.execute(sql)

    cur.close()

    # # 读取plist模版文件
    # f = open('template.plist', 'r')
    # template = f.read()
    # f.close()


def shorturl():
    c = 1
    s = ''
    cur = conn.cursor()
    while c>0:
        s = ''.join(random.sample('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',4))
        sql = "select * from %s WHERE shorturl='%s'"%(apps_new, s)
        c = cur.execute(sql)
    cur.close()
    return s

def qrcode_encode(text, path):
    qr = pyqrcode.create(text)
    qr.png(path, scale=11)

def qrcode_decode(path):
    with open(path, 'rb') as image_file:
        image = Image.open(image_file)
        image.load()

    codes = zbarlight.scan_codes('qrcode', image)
    r = codes[0]
    return r

def set_old_record(id,v=1):
    cur = conn.cursor()
    sql ="UPDATE %s SET old_record=%d WHERE id=%s"%(apps_new, v, id)
    cur.execute(sql)
    conn.commit()
    cur.close()

def sync_plus():
    error_ids = []
    cur = conn.cursor()
    sql = "ALTER TABLE `%s` ADD `updatedesc` VARCHAR(300) NOT NULL DEFAULT '' AFTER `alarmtype`,ADD `old_record` INT NOT NULL DEFAULT 0 AFTER `updatedesc`;"%plug_new
    cur.execute(sql)

    sql = "select id,name,appid,platform,parentid,versioncode,packagename,versionname,rootctrlname,icon,`desc`,updatetime,filesize,filename,filepath,downnum,filemd5,alarmtype,updatedesc,old_record from %s"%plug_new
    cur.execute(sql)
    list = cur.fetchall()
    num = 0
    for r in list:
        (id,name,appid,platform,parentid,versioncode,packagename,versionname,rootctrlname,icon,desc,updatetime,filesize,filename,filepath,downnum,filemd5,alarmtype,updatedesc,old_record)=r
        path = source_path + filepath
        file_path = path + filename
        icon_path = path + icon

        sql = "select a.id,a.`desc`,b.packagename,c.folder from tbl_plugins a INNER JOIN tbl_apps b on a.appid = b.id INNER JOIN tbl_group c ON b.gid=c.id where a.id=%s"%parentid
        c = cur.execute(sql)
        if c == 0:
            error_ids.append((id, u'row不存在:%s,%s'%(parentid,name)))
            sql ="UPDATE %s SET old_record=2 WHERE id=%s"%(plug_new, id)
            cur.execute(sql)
            continue

        (old_id,old_desc,app_packagename,folder) = cur.fetchone()


        error = False
        if len(filename)>4 and not os.path.exists(file_path):
            error = True
            error_ids.append((id, u'appid:%s,name:%s,file_path:%s'%(appid,name.decode('utf-8'),file_path)))

        if len(icon)>4 and not os.path.exists(icon_path):
            error = True
            error_ids.append((id, u'appid:%s,name:%s,icon_path路径不存在:%s'%(appid,name.decode('utf-8'),icon_path)))

        if not error:
            num = num + 1
            dest_filename = str(versioncode) + filename[-4:]
            dest = folder + os.sep + app_packagename + os.sep + 'plugs' + os.sep + packagename + os.sep
            full_dest = dest_path + dest
            if not os.path.exists(full_dest):
                os.makedirs(full_dest)

            dest_file_path = full_dest + dest_filename
            dest_icon_path = full_dest + 'icon.png'

            if not os.path.exists(dest_file_path) or os.path.getsize(dest_file_path) != os.path.getsize(file_path):
                try:
                    shutil.copy(file_path, dest_file_path)
                except:
                    print 'plus file cp failure:%s,%s'%(file_path, dest_file_path)

            if not os.path.exists(dest_icon_path) or os.path.getsize(dest_icon_path) != os.path.getsize(icon_path):
                try:
                    shutil.copy(icon_path, dest_icon_path)
                except:
                    print 'plus icon cp failure:%s,%s'%(icon_path, dest_icon_path)

            sql ="UPDATE %s SET `desc`='%s',updatedesc='%s',filepath='%s',filename='%s',old_record=1,icon='icon.png' WHERE id=%s"%(plug_new, old_desc,desc,dest,dest_file_path, id)
            cur.execute(sql)
        else:
            sql ="UPDATE %s SET old_record=2 WHERE id=%s"%(plug_new, id)
            cur.execute(sql)
        conn.commit()

    cur.close()
    print u'finish:%d'%num

    for r in error_ids:
        print '%s,%s'%(r[0], r[1])


def sync_apps():
    cur = conn.cursor()
    error_ids = []

    sql = 'SELECT a.id,a.name,a.platform,a.parentid,a.versioncode,a.versionname,a.packagename,a.icon,a.`desc`, ' \
          'a.gid,a.uid,a.updatetime,a.filesize,a.filepath,a.filename,a.downnum,a.filemd5,b.folder FROM %s a ' \
          'inner join tbl_group b on a.gid = b.id'%apps_new

    cur.execute(sql)

    list = cur.fetchall()
    num = 0
    for r in list:
        error = False

        (id,name,platform,parentid,versioncode,versionname,packagename,icon,desc,gid,uid,updatetime,filesize,filepath,filename,downnum,filemd5,folder) = r
        if folder == 'root':
            set_old_record(id, 2)
            continue

        c = cur.execute('SELECT `desc`,filename,downnum from tbl_apps where id = %s'%parentid)
        if c == 0:
            error_ids.append((id, 'row不存在:%s,%s'%(parentid,name)))
            set_old_record(id, 2)
            continue

        (old_desc,old_filename,old_downnum) = cur.fetchone()

        path = source_path + filepath
        file_path = path + filename
        icon_path = path + icon
        qrcode_path = path + 'qrcode_AppIcon.png'

        if len(filename)>4 and not os.path.exists(file_path):
            error = True
            error_ids.append((id, '文件路径不存在:%s'%file_path))

        if len(icon)>4 and not os.path.exists(icon_path):
            error = True
            error_ids.append((id, 'icon_path路径不存在:%s'%icon_path))

        if not os.path.exists(qrcode_path):
            qrcode_path = path + 'qrcode_' + icon
            if not os.path.exists(qrcode_path):
                error = True
                msg = 'qrcode路径不存在:%s,%s'%(qrcode_path,name)
                error_ids.append((id, msg))

        if not error:
            num = num + 1
            dest = folder + os.sep + packagename + os.sep
            full_dest = dest_path + dest
            if not os.path.exists(full_dest):
                os.makedirs(full_dest)

            dest_filename = str(versioncode) + filename[-4:]
            dest_file_path = full_dest + dest_filename
            dest_icon_path = full_dest  + 'icon.png'

            qrcode_text = qrcode_decode(qrcode_path)

            dest_qrcode_path = full_dest + 'qrcode.png'

            print '%s,old:%s,new:%s'%(name,file_path,dest_file_path)
            if not os.path.exists(dest_file_path) or os.path.getsize(dest_file_path) != os.path.getsize(file_path):
                try:
                    shutil.copy(file_path, dest_file_path)
                except:
                    print 'file copy failure:%s,%s'%(file_path, dest_file_path)


            if not os.path.exists(dest_icon_path) or os.path.getsize(dest_icon_path) != os.path.getsize(icon_path):
                try:
                    shutil.copy(icon_path, dest_icon_path)
                except:
                    print 'icon copy failure:%s,%s'%(icon_path,dest_icon_path)

            # 老的二维码地址，要入库，用于兼容

            # 原来iOS下载地址,如itms-services://?action=download-manifest&url=https%3A%2F%2Fwww.me-app.net%2FUploads%2FApps%2Fgongxiang%2F218%2F2_1449492992_944514825.plist
            # 提取url后面的部分
            if qrcode_text[:4] != 'http':
                u = qrcode_text.replace('itms-services://?action=download-manifest&url=', '')
                sql = "UPDATE %s SET old_down_url='%s' WHERE id=%s"%(apps_new, u, id)
                cur.execute(sql)
            elif qrcode_text[-4:] == '.apk':
                # 原来apk下载地址,如：https://www.me-app.net/Uploads/Apps/gongxiang/136/1_1446514649_250543872.apk
                # 提取 Apps后面的内容
                u = qrcode_text.replace('https://www.me-app.net/Uploads/Apps/', '')
                sql = "UPDATE %s SET old_down_url='%s' WHERE id=%s"%(apps_new, u, id)
                cur.execute(sql)

            # 重新生成新的二维码
            short_url = shorturl()
            u = 'https://dl.me-app.net/%s'%short_url
            qrcode_encode(u, dest_qrcode_path)

            # # iOS平台，生成plist文件
            # if platform == 2:
            #     dest_plist_path = full_dest + 'ios.plist'
            #     dest_plist8_path = full_dest + 'ios8.plist'
            #     body = template.replace('{url}', 'https://www.me-app.net/Uploads/%s%s'%(dest,dest_filename))
            #     body = body.replace('{icon}','https://www.me-app.net/%sicon.png'%dest)
            #     body = body.replace('{packagename}',packagename)
            #     body = body.replace('{versioncode}',str(versioncode))
            #     body = body.replace('{name}',name)
            #
            #     body_ios8 = body.replace(packagename,packagename+'_ios8')
            #
            #     f = open(dest_plist_path,'w')
            #     f.write(body)
            #     f.close()
            #
            #     f = open(dest_plist8_path,'w')
            #     f.write(body_ios8)
            #     f.close()


            sql ="UPDATE %s SET id=%s,shorturl='%s',`desc`='%s',updatedesc='%s',downnum=%s,filepath='%s',`filename`='%s',`icon`='icon.png'" \
                    " WHERE id=%s"%(apps_new, parentid, short_url, old_desc,desc,old_downnum, dest, dest_filename, id)

            cur.execute(sql)

            set_old_record(id, 1)

            # 插入更新记录到更新
            sql = "INSERT INTO %s(appid, updatedesc,updatetime) SELECT parentid,`desc`,updatetime FROM tbl_apps WHERE parentid=%s AND id<>%s AND id<>%s"%(apps_history,parentid,parentid,id)
            cur.execute(sql)

            # 插入插件记录
            sql = "INSERT INTO %s select * from tbl_plugins where id in (select max(id) from tbl_plugins where appid =%s group by packageName);"%(plug_new, parentid)
            cur.execute(sql)
        else:
            set_old_record(id, 2)

    print u'finish:%d'%num

    for r in error_ids:
        print '%s,%s'%(r[0], r[1])

    conn.commit()
    cur.close()

def main():
    """
     main
    """
    init()
    sync_apps()
    sync_plus()
    conn.close()


if __name__ == '__main__':
    main()