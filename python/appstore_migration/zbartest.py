#! /usr/bin/env python
# coding=utf-8
# -*- coding: utf-8 -*-
# date 2017/4/24
# author chenjiaxin
# version:1.0

"""
功能：

"""
from sys import argv
from PIL import Image
import zbarlight


def main():
    file_path = 'old.png'
    with open(file_path, 'rb') as image_file:
        image = Image.open(image_file)
        image.load()

    codes = zbarlight.scan_codes('qrcode', image)
    print('QR codes: %s' % codes[0])


if __name__ == '__main__':
    main()