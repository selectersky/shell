#! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-
#date 14-3-19

import os,sys,string
import eyed3

reload(sys)
sys.setdefaultencoding('utf-8')

def updateArtist(path, name):
    '''
    更新mp3文件的作者和专辑
    '''
    title = name.split('.')[0]
    full = path + os.sep + name
    upateTitle = u'%s'%title
    #print title
    #try:
    audio = eyed3.load(full)
    audio.tag.artist = upateTitle
    audio.tag.album = upateTitle
    #修改备注
    for d in audio.tag.comments:
        d.text = upateTitle

    audio.tag.save(version=eyed3.id3.ID3_DEFAULT_VERSION,encoding='utf-8')
    return True
    #except Exception, e:
        #print u'update artist failure:%s'%full
    #    return False
    
def isMp3File(path):
    '''
    根据扩展名判断当前文件是否为mp3文件
    '''
    arr = path.split('.')
    if len(arr)>1:
        name  = arr[1]
        name = name.lower()
        if name == 'mp3':
            return True
    return False

def test():
    path = u'/d/zhiwu/001被气坏的土豆地雷.mp3'
    core = eyed3.load(path)
    tag = core.tag

    for d in tag.comments:
        d.text = u'中国人'

    #print '%s'%tag.comments.get(u'artist')
    #tag.artist =u'中国人' 
    #tag.setTextEncoding('utf-8')
    tag.save(version=eyed3.id3.ID3_DEFAULT_VERSION,encoding='utf-8')
    #print tag.artist
    #print core

def main():
    if len(sys.argv)<2:
        path = './'
    else:
        path = sys.argv[1]
        if not os.path.exists(path):
            print u'path %s not exists!'%path
            return
    num = 0
    count = 0
    for file in os.listdir(path):
        if isMp3File(file):
            count += 1
            if updateArtist(path.rstrip(os.sep), file):
                num += 1
                print u'update %s artist successed'%file
            else:
                print u'update %s artist failure'%file
            
    print 'finished,count: %d,successed:%d'%(count,num)

if __name__ == '__main__':
    main()
    #test()