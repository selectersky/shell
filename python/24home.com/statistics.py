#! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-
import string,thread,time
"""
功能：将24home.com网站上的所有低于50元且有货的产品生成到24home.html文件中

执行过程中，按q+回车将中止执行，并将已经获取到的结束写到24home.html文件中
"""
from Cjx.http import httpCient
__author__ = 'appie'

isExit = False

def myExit():
    a = raw_input()
    while a != 'q' and a != 'exit' and a != 'bye' and a != 'quit':
        print 'a=%s'%a
        a = raw_input()
    global isExit
    isExit = True

def sortKey(x):
    return x["price"]

def main():

    #代理
    proxy = "127.0.0.1:8087"
    http = httpCient(True, None, proxy)

    #获取分类详细
    url = "http://www.24home.com/commonAjax/getNavigation.htm"
    header = {
        "Referer":"http://www.24home.com/"
    }
    content = http.urlcontent(url, None, header)
    #分类匹配正则
    repxStr = "http://www\.24home\.com/productList/([0-9]*?)-0-0-0-4-0-0-0-1\.htm"
    catelist = http.categroyList(content, repxStr)
    if len(catelist)<=0:
        print("get category failed!")
        return
    print 'get category count:', len(catelist)
    #产品地址格式 3-0-49-1 3代表含义是按价格升序 49-1代表1-49元价格的产品
    urlFormat = "http://www.24home.com/productList/%d-0-0-0-3-0-69-1-%d.htm"
    #每条产品信息正则
    repxStr = "j_normal(.*?)j_shadow"
    #总共分页数正则
    pageCountRepxStr = "共([0-9]*?)页"
    #要获取的产品信息正则
    detailDict = {
        'price':'<span>(.*?)</span>',
        'url':'href=\"(.*?)\"',
        'title':'title=\"(.*?)\"',
        'img':'src=\"(.*?)\"'
    }
    #启动一个线程监视用户输入 可以输入q+回车中止执行
    thread.start_new_thread(myExit, ())
    alllist = []
    #罗列分类
    for cateid in catelist:
        page = 1
        pagecount = 0
        try:
            categoryid = string.atoi(cateid)
        except:
            print 'covert category identity failed:',cateid
            continue
        #循环所分页
        while (page <= pagecount or pagecount == 0) and isExit == False:
            url = urlFormat % (categoryid, page)
            content = http.urlcontent(url)
            #获取所有的产品列表
            list = http.itemlist(content, repxStr, pagecount, pageCountRepxStr)
            #转换分页
            if pagecount == 0:
                try:
                    pagecount = string.atoi(list['pagecount'])
                except:
                    pagecount = 1
                    print 'covert page count failed:', list['pagecount']
            #处理每个产品数据
            pro_data = list['list']
            if len(pro_data) > 0:
                print 'url ', url, ' count ',len(pro_data)
                for item in pro_data:
                    #获取产品详细信息
                    detail = http.itemDetail(item, detailDict)
                    #转换价格
                    try:
                        detail['price'] = string.atof(detail['price'])
                    except:
                        print 'covert price failed,price:', detail['price'], ',name:', detail['title'], ',url:', detail['url']
                    #压入数组
                    alllist.append(detail)
            page = page + 1
        if isExit == True:
            break

    #数组排序
    alllist.sort(key=sortKey)

    fmt = "" \
          "<td>%.2f</td>" \
          "<td><a href=\"%s\" target=\"_blank\">%s</a></td>" \
          "<td><img src=\"%s\" /></td>" \
           ""
    i = 0
    count = len(alllist)

    print 'get product count:', count

    content = ""
    #循环数组存入内存字符串
    while i < count:
        tmp = "<tr>"
        v = alllist[i]
        try:
            item = fmt % (v["price"], v["url"], v["title"], v["img"])
            tmp = tmp + item
        except:
            print 'format failed'
        #下一个产品
        i = i + 1
        if i < count:
            v = alllist[i]
            try:
                item = fmt % (v["price"], v["url"], v["title"], v["img"])
                tmp = tmp + item
            except:
                print 'format failed'
            i = i + 1
        tmp = tmp + "</tr>"
        content = content + tmp

    totalStr = "<tr><td colspan=\"6\" align=\"left\">分类总数：<strong>%d</strong>  产品总数：<strong>%d</strong> 最新更新时间：<strong>%s</strong></td></tr>" % (len(catelist),count,time.strftime('%Y-%m-%d %H:%M:%S'))
    content = content + totalStr
    #打开模版文件
    fp = open("mode.html", "r")
    mode = fp.read()
    fp.close()

    #输出到文件
    content = mode.replace("{list}", content)
    fp = open("24home.html", "w")
    fp.write(content)
    fp.close()

if __name__ == '__main__':
    main()

