# ! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-
#date 15-9-30
"""
功能：公司考勤表格在MAC下打开非常慢，使用这个工具可以快速查询考勤信息

1.0 2015.09.30 完成基本功能，支持按工号和姓名查询考勤数据
1.1 2015.10.08 解决读取数据文件时间显示不对问题；增加显示星期的功能
1.2 2015.10.08 增加显示加班数，增加统计迟到次数；解决统计忘打卡多一次的问题
1.3 2015.10.09 增加统计加班小时数功能

1.4 计划 增加迟到高度显示，增加图表曲线统计。支持windows版本

%y 两位数的年份表示（00-99）
%Y 四位数的年份表示（000-9999）
%m 月份（01-12）
%d 月内中的一天（0-31）
%H 24小时制小时数（0-23）
%I 12小时制小时数（01-12）
%M 分钟数（00=59）
%S 秒（00-59）
%a 本地简化星期名称
%A 本地完整星期名称
%b 本地简化的月份名称
%B 本地完整的月份名称
%c 本地相应的日期表示和时间表示
%j 年内的一天（001-366）
%p 本地A.M.或P.M.的等价符
%U 一年中的星期数（00-53）星期天为星期的开始
%w 星期（0-6），星期天为星期的开始
%W 一年中的星期数（00-53）星期一为星期的开始
%x 本地相应的日期表示
%X 本地相应的时间表示
%Z 当前时区的名称
%% %号本身

"""

import xlrd
import xlwt
import os,time,datetime,string
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

#获取当前目录下最新的excel文件
def lastxmlx():
    path = sys.argv[0]
    arr = path.split(os.sep)
    if len(arr)>1:
        path = path[:path.rindex(os.sep)]
    else:
        path = './'
    print u'path:%s\n'%path
    #print u'path:%s,%s,%s,%s,%s,%s'%(path,os.path.realpath(__file__),__file__,os.getcwd(),sys.path[0],sys.argv[0])
    findpath = None
    name = None
    timeStamp = 0
    for files in os.listdir(path):
        arr = files.split('.')
        if len(arr) > 1 and (arr[1] == 'xlsx' or arr[1] == 'xls'):
            fullpath = path + os.sep + files
            curtime = os.path.getmtime(fullpath)
            if timeStamp < curtime:
                timeStamp = curtime
                findpath = fullpath
                name = files
    if timeStamp:
        return [timeStamp,name,findpath]
    else:
        return None

def isLast(time_):
    tarr = time_.split(':')
    if tarr[0]>'09':
        return True
    elif tarr[0] == '09' and tarr[1]>'00':
        return True
    return False

def main():
    print u'---------------------------\n大伯瑞辅助考勤查询小工具 v1.3(20151009)\n---------------------------\n'
    f = lastxmlx()
    if not f:
        t = raw_input(u'亲，当前目录没有找到考勤数据库文件，请到公司群下载最新的考勤数据表\n')
        return

    timeStamp = f[0]
    name = f[1]
    path = f[2]

    localtime = time.localtime(timeStamp)
    timeStr = time.strftime("%Y-%m-%d %H:%M:%S",localtime)
    print u'数据文件:%s，更新日期：%s\n\n---------------------------'%(name,timeStr)
    inputStr = raw_input(u"请输入你的工号或姓名:")
    inputStr = inputStr.decode('utf-8')
    '''
    序号 0,
考勤号码 1,
自定义编号 2,
姓名 3,
是否智能排班 4,
日期 5,
对应时段 6,
上班时间 7,
下班时间 8,
签到时间 9,
签退时间 10,
应到 11,
实到 12,
迟到时间 13,
早退时间 14,
是否旷工 15,
加班时间 16,
工作时间 17,
例外情况 18,
应签到 19,
应签退 20,
部门 21,
平日 22,
周末 23,
节假日 24,
出勤时间 25,
平日加班 26,
周末加班 27,
节假日加班 28,
'''
    print u'%4s%6s%12s%7s%7s%4s%6s%4s%4s%4s\n' % (u'工号',u'姓名',u'日期',u'上班',u'下班',u'类型',u'部门',u'平日加班',u'周末加班',u'节假日加班')
    xls = xlrd.open_workbook(path)
    table = xls.sheets()[0]
    #早上缺勤
    upNum = 0
    #下午缺勤
    downNum = 0
    #迟到缺勤
    lateNum = 0
    #工作日总数
    daynum = 0
    #平时加班时间
    overtime_normal_num = 0.0
    #周末加班时间
    overtime_sun_num = 0.0
    #节假日加班时间
    overtime_free_num = 0.0
    t2 = datetime.datetime.now()
    #当前天数 用于比较指定日期是否在当天
    curDayOfYear = t2.strftime('%j')
    for i in range(table.nrows ):
        
        r = table.row_values(i)

        #工号
        num = r[1]
        #姓号
        name = r[3]
        date = r[5]

        #上班打卡时间
        up = r[9]
        
        #下班打卡时间
        down = r[10]

        #部门
        department = r[21]
        #是否是工作日
        workday = r[22]
        #是否是休息日
        satday = r[23]
        #是否是节假日
        freeday = r[24]

        #平日加班
        overtime_normal = r[26]
        
        #周末加班
        overtime_sun = r[27]
        
        #节假日加班
        overtime_free = r[28]
        

        #if i <= 10:
           # print u'num=%s,name=%s,date=%s,up=%s,down=%s,department=%s,workday=%s,satday=%s,freeday=%s,overtime_normal=%s,overtime_sun=%s,overtime_free=%s'%(num,name,date,up,down,department,workday,satday,freeday,overtime_normal,overtime_sun,overtime_free)

        
        if num == inputStr or name == inputStr:
            if not overtime_normal:
                overtime_normal = '-'
            else:
                overtime_normal_num = overtime_normal_num + float(overtime_normal)

            if not overtime_sun:
                overtime_sun = '-'
            else:
                overtime_sun_num = overtime_sun_num + float(overtime_sun)

            if not overtime_free:
                overtime_free = '-'
            else:
                overtime_free_num = overtime_free_num + float(overtime_free)

            dayname = u'工作日'
            if workday != '1':
                dayname = u'休息日'
            else:
                daynum = daynum + 1
                
                if up and isLast(up):
                    lateNum = lateNum + 1
            t =  time.strptime(date,'%Y/%m/%d')
            dayOfyear = time.strftime('%j',t)
            datename = u'%s(%s)'%(date, time.strftime('%a',t))
            #如果不是今天的数据，则处理
            if dayOfyear != curDayOfYear:
                if not up:
                    up = '-'
                    if workday == '1':
                            upNum = upNum + 1 
                    down = '-'
                    if workday == '1':
                        downNum = downNum + 1
            #print u'num=%s,name=%s,date=%s,up=%s,down=%s,department=%s,workday=%s,satday=%s,freeday=%s,overtime_normal=%s,overtime_sun=%s,overtime_free=%s'%(num,name,date,up,down,department,workday,satday,freeday,overtime_normal,overtime_sun,overtime_free)
            print u"%4s%6s%12s%7s%7s%4s%6s%4s%4s%4s\n"%(num,name,datename,up,down,dayname,department,overtime_normal,overtime_sun,overtime_free)

    str = u''
    if upNum > 0:
        str = str + u' 上班忘打卡%d次'%upNum
    if downNum > 0:
        str = str + u' 下班忘打卡%d次'%downNum
    if lateNum > 0:
        str = str + u' 早上迟到%d次'%lateNum
    if overtime_normal_num > 0.0:
        str = str + u' 平时加班共%.2f小时'%overtime_normal_num
    if overtime_sun_num > 0.0:
        str = str + u' 周末加班共%.2f小时'%overtime_sun_num
    if overtime_free_num > 0.0:
        str = str + u' 节假日加班共%.2f小时'%overtime_free_num
    if str:
        print u"%s\n"%str
    t = raw_input()


if __name__ == '__main__':
    main()
