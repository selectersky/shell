# ! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-
#date 15-9-30
"""
功能：公司考勤表格在MAC下打开非常慢，使用这个工具可以快速查询考勤信息

1.0 2015.09.30 完成基本功能，支持按工号和姓名查询考勤数据
1.01 2015.10.08 解决读取数据文件时间显示不对问题；增加显示星期的功能
1.02 2015.10.08 增加显示加班数，增加统计迟到次数；解决统计忘打卡多一次的问题
1.03 2015.10.09 增加统计加班小时数功能
1.05 2015.10.09 使用标准数据统计迟到、早迟，统计更准确；增加显示班次；增加旷工、早退统计；去掉加班详细
1.06 2015.10.13 兼容windows
1.1.0 2015.12.21 支持重复查询，支持汇总表查询

%y 两位数的年份表示（00-99）
%Y 四位数的年份表示（000-9999）
%m 月份（01-12）
%d 月内中的一天（0-31）
%H 24小时制小时数（0-23）
%I 12小时制小时数（01-12）
%M 分钟数（00=59）
%S 秒（00-59）
%a 本地简化星期名称
%A 本地完整星期名称
%b 本地简化的月份名称
%B 本地完整的月份名称
%c 本地相应的日期表示和时间表示
%j 年内的一天（001-366）
%p 本地A.M.或P.M.的等价符
%U 一年中的星期数（00-53）星期天为星期的开始
%w 星期（0-6），星期天为星期的开始
%W 一年中的星期数（00-53）星期一为星期的开始
%x 本地相应的日期表示
%X 本地相应的时间表示
%Z 当前时区的名称
%% %号本身

"""

import xlrd
import os,time,string,platform
import sys
reload(sys)
charset = u'utf-8'
version = u'1.1.0'
updateDate = u'2015-12-20'
if platform.system() == u'Windows':
    charset = u'gb2312'
sys.setdefaultencoding(charset)

def comp(x, y):
    '''
    降序排序
    :param x:
    :param y:
    :return:
    '''
    if x['mtime'] < y['mtime']:
        return 1
    elif x['mtime'] > y['mtime']:
        return -1
    else:
        return 0
# 获取当前目录下最新的excel文件
def lastxmlx():
    '''
    读取目录下的excel文件列表
    :return:  数组
    '''
    path = sys.argv[0]
    arr = path.split(os.sep)
    if len(arr) > 1:
        path = path[:path.rindex(os.sep)]
    else:
        path = './'
    print u'path:%s\n' % path
    # print u'path:%s,%s,%s,%s,%s,%s'%(path,os.path.realpath(__file__),__file__,os.getcwd(),sys.path[0],sys.argv[0])
    result = []
    for files in os.listdir(path):
        arr = files.split('.')
        if len(arr) < 1:
            continue
        ext = arr[len(arr) - 1]

        if len(arr) > 1 and (ext == 'xlsx' or ext == 'xls'):
            fullpath = path + os.sep + files
            curtime = os.path.getmtime(fullpath)
            result.append({'name': files, 'mtime': curtime, 'full': fullpath})
    return result

def strtime(timestamp):
    '''
    格式化时间
    :param timestamp:时间戳
    :return: 格式化后的字符串时间
    '''
    localtime = time.localtime(timestamp)
    timeStr = time.strftime("%Y-%m-%d %H:%M:%S", localtime)
    return timeStr

def progressByStatistics(table, inputStr):
    for i in range(table.nrows ):
        r = table.row_values(i)
        s = u''
        for j in r:
            if s == u'':
                s = u'%s' % j
            else:
                s = s + u',%s' % j
        print u'count:%d\n%s\n' % (len(r), s)
        if i > 5:
            break

def progressByDetail(table, inputStr):
    '''
    处理详细考勤表
    :param table: 表
    :param inputStr: 要查询的关键字
    :return:
    '''
    print u'%4s%6s%15s%6s%7s%7s%4s%6s\n' % (u'工号', u'姓名', u'日期', u'时段', u'上班', u'下班', u'实到', u'部门')
    #早上缺勤
    upNum = 0
    #下午缺勤
    downNum = 0
    #迟到缺勤
    lateNum = 0
    #早迟次数
    preNum = 0
    #工作日总数
    daynum = 0
    #旷工次数
    absentNum = 0
    #平时加班时间
    overtime_normal_num = 0.0
    #周末加班时间
    overtime_sun_num = 0.0
    #节假日加班时间
    overtime_free_num = 0.0
    #表示是否有找到数据
    findnum = 0
    inputStrNum = inputStr + u'.0'
    for i in range(table.nrows ):

        r = table.row_values(i)
        '''
            序号 0,
        考勤号码 1,
        自定义编号 2,
        姓名 3,
        是否智能排班 4,
        日期 5,
        对应时段 6,
        上班时间 7,
        下班时间 8,
        签到时间 9,
        签退时间 10,
        应到 11,
        实到 12,
        迟到时间 13,
        早退时间 14,
        是否旷工 15,
        加班时间 16,
        工作时间 17,
        例外情况 18,
        应签到 19,
        应签退 20,
        部门 21,
        平日 22,
        周末 23,
        节假日 24,
        出勤时间 25,
        平日加班 26,
        周末加班 27,
        节假日加班 28,
        '''
        #工号
        num = '%s' % r[1]
        #姓号
        name = r[3]
        #时间
        date = r[5]
        #对应时段
        dateType = r[6]
        #上班时间
        uptime = r[7]
        #下班时间
        downtime = r[8]

        #上班打卡时间
        up = r[9]

        #下班打卡时间
        down = r[10]
        #应到
        needtime = r[11]
        #实到
        yestime = r[12]
        #迟到时间
        latetime = r[13]
        #早退时间
        pretime = r[14]
        #是否旷工
        absent = r[15]


        #部门
        department = r[21]
        #是否是工作日
        workday = r[22]
        #是否是休息日
        satday = r[23]
        #是否是节假日
        freeday = r[24]

        #平日加班
        overtime_normal = r[26]

        #周末加班
        overtime_sun = r[27]

        #节假日加班
        overtime_free = r[28]


        #if i <= 10:
        # print u'num=%s,name=%s,date=%s,up=%s,down=%s,department=%s,workday=%s,satday=%s,freeday=%s,overtime_normal=%s,overtime_sun=%s,overtime_free=%s,dateType=%s,uptime=%s,downtime=%s,yestime=%s,latetime=%s,pretime=%s'%(num,name,date,up,down,department,workday,satday,freeday,overtime_normal,overtime_sun,overtime_free,dateType,uptime,downtime,yestime,latetime,pretime)

        if num == inputStr or name == inputStr or num.split('.')[0] == inputStr:
            findnum = findnum + 1
            t =  time.strptime(date,'%Y/%m/%d')
            datename = u'%s(%s)'%(date, time.strftime('%a',t))

            if not overtime_normal:
                overtime_normal = '-'
            else:
                overtime_normal_num = overtime_normal_num + float(overtime_normal)

            if not overtime_sun:
                overtime_sun = '-'
            else:
                overtime_sun_num = overtime_sun_num + float(overtime_sun)

            if not overtime_free:
                overtime_free = '-'
            else:
                overtime_free_num = overtime_free_num + float(overtime_free)

            #迟到次数
            if latetime:
                lateNum = lateNum + 1
            #早退次数
            if pretime:
                preNum = preNum + 1
            #旷工次数
            if absent:
                absentNum = absentNum + 1

            yestimename = u'Y'
            if not yestime:
                yestimename = u'-'
            else:
                daynum = daynum + 1


            if not up:
                up = '-'
                if yestime:
                    upNum = upNum + 1
            if not down:
                down = '-'
                if yestime:
                    downNum = downNum + 1
            #print u'num=%s,name=%s,date=%s,up=%s,down=%s,department=%s,workday=%s,satday=%s,freeday=%s,overtime_normal=%s,overtime_sun=%s,overtime_free=%s,dateType=%s,uptime=%s,downtime=%s,yestime=%s,lasttime=%s,pretime=%s'%(num,name,date,up,down,department,workday,satday,freeday,overtime_normal,overtime_sun,overtime_free,dateType,uptime,downtime,yestime,lasttime,pretime)
            print u"%4s%6s%15s%6s%7s%7s%4s%6s\n"%(num,name,datename,dateType,up,down,yestimename,department)

    str = u''
    if upNum > 0:
        str = str + u' 上班忘打卡%d次'%upNum
    if downNum > 0:
        str = str + u' 下班忘打卡%d次'%downNum
    if lateNum > 0:
        str = str + u' 上班迟到%d次'%lateNum
    if preNum > 0:
        str = str + u' 下班早退%d次'%preNum
    # 当前旷工原始数据有误，不显示
    # if absentNum > 0:
    #     str = str + u' 旷工%d次'%absentNum
    if overtime_normal_num > 0.0:
        str = str + u' 平时加班共%.2f小时'%overtime_normal_num
    if overtime_sun_num > 0.0:
        str = str + u' 周末加班共%.2f小时' % overtime_sun_num
    if overtime_free_num > 0.0:
        str = str + u' 节假日加班共%.2f小时' % overtime_free_num
    if str:
        print u"%s\n" % str
    if not findnum:
        print u'查无相关记录\n'

def progress(dblist):
    count = len(dblist)
    for i in range(count):
        item = dblist[i]
        print u'[%d] %s (%s)\n' % (i, item['name'], strtime(item['mtime']))

    n = raw_input(u'请输入数据库文件序号[0-%d]:' % (count - 1))
    if not n:
        n = 0
    n = int(n)
    if n < 0 or n >= count:
        n = 0

    item = dblist[n]
    print u'数据文件:%s，更新日期：%s\n\n---------------------------'%(item['name'], strtime(item['mtime']))
    path = item['full']
    inputStr = raw_input(u"请输入你的工号或姓名:")

    inputStr = inputStr.decode(charset)
    xls = xlrd.open_workbook(path)
    table = xls.sheets()[0]
    # progressByStatistics(table,inputStr)
    progressByDetail(table, inputStr)
def main():
    print u'---------------------------\n大伯瑞辅助考勤查询小工具 v%s(%s)\n---------------------------\n' % (version, updateDate)
    list = lastxmlx()
    if len(list) == 0:
       raw_input(u'亲，当前目录没有找到考勤数据库文件，请到公司群下载最新的考勤数据表\n')
       return 0
    list.sort(comp)

    r = ''
    while r.lower() != 'n':
        progress(list)
        r = raw_input(u'是否要继续查询?(y/n):')

if __name__ == '__main__':
    main()
