# Abc360 考勤辅助查询小工具

## 起因
abc360的考勤数据使用excel表格制定，然后使用VBScript脚本实现输入工号进行查询。在windows下，输入工号后查询自己的考勤数据，大概需要5秒左右时间，加上打开时间，一般在10秒左右时间；而在mac系统下，这个查询脚本执行非常慢，几乎等了半个小时，数据还是显示不出来。

## 解决
使用**Python**脚本，依赖**xlrd**插件，循环读取excel表格的数据，然后将指定考勤数据显示出来

## 重要特性
1. 能够使用工号或姓名进行查询
2. 查询时间很快，5秒内能有结果
3. 具有基本的统计功能。重要的考勤数据一目了然
4. 可以在mac下和windows下使用

## 运行和打包
### 运行
需要安装第三方组件 **xlrd** 才能使用`python`命令执行。如果安装了pip，可使用如下命令快速安装：

		$pip install xlrd
		$python abc360.py

如果你没有安装pip，请自行去<https://pypi.python.org/pypi/pip>下载安装。

如果安装时出现权限错误，请使用`sudo`执行安装命令。

### 打包
使用**Python**插件`pyinstaller`可以将当前脚本打包成功一个可执行文件。使用**pip**可快速安装这个插件：
		
		$pip install pyinstaller
**pyinstaller**安装成功后，执行如下命令进行打包：
		
		$pyinstaller -F abc360.py
		
**pyinstaller**命令行手册参考<http://pythonhosted.org/PyInstaller/>