#!/bin/sh

projectDir='/d/android/ivms5060/4.2/iVMS-5060V4.2.0_AS/Code_AS'
oldname=com.ivms
newname=com.ivms.chenjiaxin

prefix='tmp'
if [ ! -n "$projectDir" ] ;then
    echo "Pls input projectDir!"
    exit 1
fi

if [ ! -n "$oldname" ] ;then
    echo "Pls input old packageName!"
    exit 1
fi

if [ ! -n "$newname" ] ;then
    echo "Pls input new packageName!"
    exit 1
fi

if [ $oldname = $newname ]; then
	echo 'the same packageName' 
	exit 1
fi

firstname=${oldname%%\.*}
srcfrom=${oldname//.//}
srcto=$prefix${newname//.//}

cd $projectDir
files=`ls src/$srcfrom`
if [ -z "$files" ]; then
	echo "no files found in path:$srcfrom,Please make sure your path is correct"
	exit 1
fi

# 处理包名
sed -i '' -e "s/$oldname/$newname/g" `grep "$oldname" -l AndroidManifest.xml` || echo "Not found packageName:$oldname" || exit 1
sed -i '' -e "s/$oldname/$newname/g" `grep "$oldname" -rl src`
sed -i '' -e "s/$oldname/$newname/g" `grep "$oldname" -rl res`

# 处理文件夹
cd $projectDir/src
rm -rf $srcto
mkdir -p $srcto
cp -Rf $srcfrom/* $srcto
rm -rf $firstname

s=$prefix$firstname
t=$firstname
mv $s $t

exit 0