#!/bin/sh


#工程目录名称，为svn/目录下的名称
projectname=$1

#Release
configuration=$2

buildid=$3

projectpath=$4

llvm=/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/llvm-cov
scan=/d/soft/checker-278/bin

# 脚本目录
root=$(cd `dirname $0`; pwd)
cd "$root"
# 工程目录
project="$root/svn/$projectname"

if [ -d "$projectpath" ]; then
	project=$projectpath
fi

xcodeproj="$project/${projectname}.xcodeproj"

scheme="${projectname}"

# 编译目录
buildpath="$project/build"
deviceData="$project/deviceData"

# 生成打包文件
curDate=$(date +%Y%m%d)

# 生成目标目录
dist="$root/dist/$projectname/$buildid"
dsym="$dist/$projectname-$buildid-$configuration-dsym.zip"
releaseZip="$dist/$projectname-$buildid-$configuration.zip"

rm -rf "$dist"

# 创建目录
mkdir -p "$dist"

# 单元测试结果文件
unit_test_result="$dist/unit_test_result.txt"

analyzerpath="$dist/analyzer"


# 清除上一次编译
xcodebuild -project "$xcodeproj" -target "$scheme" -configuration "$configuration" TARGET_BUILD_DIR="$buildpath" clean
rm -rf $buildpath
rm -rf "$deviceData"

# 打包
xcodebuild -project "$xcodeproj" -target "$scheme" -configuration "$configuration" TARGET_BUILD_DIR="$buildpath" || exit 1

cd $buildpath
if [ "$configuration" == "Release" ];then 
	# Release 下，打包dsym文件
	zip -rq "$dsym" Release-iphoneos/*.dSYM
fi

zip -rq "$releaseZip" *.framework

# 单元测试
xcodebuild test -project "$xcodeproj" -scheme "${scheme}" -enableCodeCoverage YES VALID_ARCHS=x86_64 CONFIGURATION_BUILD_DIR="$buildpath" -derivedDataPath "$deviceData" -destination 'platform=iOS Simulator,name=iPhone 6s,OS=10.2'
$llvm report -instr-profile "$deviceData/Build/Intermediates/CodeCoverage/Coverage.profdata" "$buildpath/$scheme.framework/$scheme" >>$unit_test_result

# 静态检查
$scan/scan-build -o "$analyzerpath" --use-analyzer Xcode xcodebuild -project "$xcodeproj" -scheme "$scheme" || exit 1
# 最后显示单元测试结果
cat $unit_test_result
exit 0