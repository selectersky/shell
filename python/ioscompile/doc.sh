#!/bin/sh
# 在当前目录html文件夹生成api文档
# framework的完整路径 如：/d/xx.framework 
path=$1

ext=${path#*"."}
name=${path%"."*}

if [ "$ext" != "framework" ]; then
	echo "示例：${0} /d/xxx.framework"
	exit 1
fi

appledoc --project-name $name --ignore "*-umbrella.h" --no-create-docset --project-company "HIKVISION" --docset-copyright "HIKVISION copyright" --docset-feed-name "HIKVISION" --docset-feed-url "https://www.me-app.net" ${path}/Headers/*.h