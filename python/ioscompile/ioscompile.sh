#!/bin/sh

#工程目录名称，为svn/目录下的名称
projectname=$1

#Release
configuration=$2

buildid=$3

autoSubmitGit=$4

runTest=$5

projectpath=$6

llvm=/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/llvm-cov
scan=/d/soft/checker-278/bin

# 脚本目录
root=$(cd `dirname $0`; pwd)
cd "$root"
# 工程目录
project="$root/svn/$projectname"

if [ -d "$projectpath" ]; then
	project=$projectpath
fi

hkgit="$root/hkgit"
workspaceName="$project/MApp.xcworkspace"

scheme="MAppPlatform"
plistpath="$project/$scheme/$scheme/Info.plist"
# 编译目录
buildpath="$project/Build"
buildDebugPath="$project/DebugBuild"
deviceData="$project/deviceData"

# 生成打包文件
archivePath="$buildpath/MeApp-$configuration.xcarchive"
curDate=$(date +%Y%m%d)

# 生成目标目录
dist="$root/dist/$projectname/$buildid"

rm -rf "$dist"

# 创建目录
mkdir -p "$dist"

# ipa目录
ipa="$dist/$scheme-$buildid-$configuration.ipa"


# 单元测试结果文件
unit_test_result="$dist/unit_test_result.txt"

analyzerpath="$dist/analyzer"

# dsym 文件
dsym="$dist/$scheme-$buildid-$configuration-dsym.zip"

releaseZip="$dist/MAppRelease-$buildid-$configuration.zip"
# 框架为frameworks，视频、文件、地图、消息插件为源码
releaseFullZip="$root/MAppRelease-$configuration-full.zip"

# 证书必须在xcode里面设置好
# deveCodeSignIdentity="iPhone Developer: 琦 宋 (TRWTBPAGG3)"
# deveProvisioningProfile="f28d8731-1659-442d-a2e5-cebcf0b471a2"

# adHocCodeSignIdentity="iPhone Distribution: Hangzhou Hikvision Digital Technology Co., Ltd."
# adHocProvisioningProfile="d1e1539f-24ad-44a6-94de-0577673593f0"

# codeSignIdentity=$deveCodeSignIdentity
# provisioningProfile=$deveProvisioningProfile

# if [ "$configuration" == "Release" ];then 
# 	codeSignIdentity=$adHocCodeSignIdentity
# 	provisioningProfile=$adHocProvisioningProfile
# fi

#设置编译时间
/usr/libexec/PlistBuddy -c "Set CFBundleVersion $curDate" "$plistpath"

# 更新pods
cd "$project"
cp -f Podfile_default Podfile
rm -f Podfile.lock
pod install

# 清除上一次编译
rm -rf "$deviceData"
xcodebuild -workspace "$workspaceName" -scheme "$scheme" -configuration "$configuration" CONFIGURATION_BUILD_DIR="$buildpath" -derivedDataPath "$deviceData" clean
rm -rf $buildpath
rm -rf $buildDebugPath

# 打包
# 先编译一个模拟器版本
xcodebuild -workspace "$workspaceName" -scheme "$scheme" -configuration "debug" -sdk iphonesimulator CONFIGURATION_BUILD_DIR="$buildDebugPath" -derivedDataPath "$deviceData" || exit 1
xcodebuild -workspace "$workspaceName" -scheme "$scheme" -configuration "$configuration" CONFIGURATION_BUILD_DIR="$buildpath" -derivedDataPath "$deviceData" || exit 1

xcrun -sdk iphoneos PackageApplication -v  "$buildpath/$scheme.app" -o  "$ipa"

cd "$buildpath"
# Debug 下 运行自动化脚本测试 test
if [ "$configuration" == "Release" ];then 
	# Release 下，打包dsym文件
	zip -rq "$dsym" *.dSYM
fi

# 增加framework模拟器支持,自动提交更新到git仓库
for dir in $(ls $buildDebugPath)
do
	ext=${dir#*"."}
	name=${dir%"."*}
	if [ "$ext" == "framework" ];then
		sour="$buildDebugPath/$dir/$name"
		dest="$buildpath/$dir/$name"
		lipo -create $sour $dest -output $dest || exit 1
	fi
	
	if [ "$autoSubmitGit" == "false" ]; then
		continue
	fi

	gitpath="$hkgit/$name"
	if [ -d "$gitpath" ]; then
		cd "$gitpath"
		# 删除CocoaPods缓存
		rm -rf ~/Library/Caches/CocoaPods/Pods/Specs/Release/Fsdmdept+${name}
		rm -rf ~/Library/Caches/CocoaPods/Pods/Specs/External/Fsdmdept+${name}

		#rm -rf ~/Library/Caches/CocoaPods/Pods/Release/Fsdmdept+${name}
		#rm -rf ~/Library/Caches/CocoaPods/Pods/External/Fsdmdept+${name}
		git pull
		cp -Rf $buildpath/$dir $gitpath/
		appledoc --project-name $name --ignore "*-umbrella.h" --no-create-docset --project-company "HIKVISION" --docset-copyright "HIKVISION" --docset-feed-name "HIKVISION" --docset-feed-url "https://www.me-app.net" ${name}.framework/Headers/*.h
		
		git add *
		git commit -m "Auto $curDate"
		git push origin master
	fi
done   

# 更新提供给行业的pods
pod repo update HKSpecs
cd "$project"
cp -f Podfile_base Podfile
rm -f Podfile.lock
rm -rf Pods
pod install

# 打包提供给行业的zip包 行业开发包默认不会有业务相关的插件，只有一个纯框架
zip -9rq "$releaseZip" MApp.xcworkspace MAppPlatform Podfile Podfile.lock Pods || exit 1

# 生成完整的frameworks
cd "$buildpath"

# 使用的Pofile_Full重新生成工程，这个压缩提供给综合安防，开放业务相关的源代码
cd "$project"
cp -f Podfile_full Podfile
# 删除lock文件主要是考虑组件可能有更新的情况
rm -f Podfile.lock
pod install

# 打包
zip -9rq "$releaseFullZip" MAppPlatform Plugins/FileManagerPlugin Plugins/MapPlugin Plugins/MessagePlugin Plugins/VideoPlayPlugin Plugins/MSPPNetSDK Plugins/HKGXToolbox MApp.xcworkspace Podfile Podfile.lock Pods || exit 1

# 单元测试
cp -f Podfile_default Podfile
rm -f Podfile.lock
pod install
# 清除上一条编译数据
#rm -rf "$buildpath"

# name="VideoPlayPlugin"
# xcodebuild test -workspace "$workspaceName" -scheme "$name"  -configuration "Debug" -sdk iphonesimulator -enableCodeCoverage YES CONFIGURATION_BUILD_DIR="$buildpath" -derivedDataPath "$deviceData" -destination 'platform=iOS Simulator,name=iPhone 6s,OS=10.0'
# $llvm report -instr-profile "$deviceData/Build/Intermediates/CodeCoverage/Coverage.profdata" "$buildpath/$name.framework/$name" >>$unit_test_result

if [ "$runTest" == "false" ]; then
	exit 0
fi

name="PluginManagerCore"
# 更新pods
cd "$project/Plugins/$name/Demo/"
rm -f Podfile.lock
pod install

workspace="$project/Plugins/$name/Demo/${name}.xcworkspace"
xcodebuild test -workspace "$workspace" -scheme "${name}" -configuration "debug" -sdk iphonesimulator -enableCodeCoverage YES VALID_ARCHS=x86_64 CONFIGURATION_BUILD_DIR="$buildpath" -derivedDataPath "$deviceData" -destination 'platform=iOS Simulator,name=iPhone 6s,OS=10.2'
$llvm report -instr-profile "$deviceData/Build/Intermediates/CodeCoverage/Coverage.profdata" "$buildpath/$name.framework/$name" >>$unit_test_result

# 静态检查
# 清除上一次编译
# name="VideoPlayPlugin"
# xcodebuild -workspace "$workspaceName" -scheme "$name" -configuration "$configuration" CONFIGURATION_BUILD_DIR="$buildpath" -derivedDataPath "$deviceData" clean
# $scan/scan-build -o "$analyzerpath" --use-analyzer Xcode xcodebuild  -workspace "$workspaceName" -scheme "$name" || exit 1

# name="PluginManagerCore"
# workspace="$project/Plugins/$name/Demo/$name.xcworkspace"
# xcodebuild -workspace "$workspace" -scheme "$name" -configuration "$configuration" CONFIGURATION_BUILD_DIR="$buildpath" -derivedDataPath "$deviceData" clean
# $scan/scan-build -o "$analyzerpath" --use-analyzer Xcode xcodebuild  -workspace "$workspaceName" -scheme "$name" || exit 1

# 最后显示单元测试结果
cat $unit_test_result
exit 0