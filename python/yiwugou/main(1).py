#! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-

"""
功能：使用当前目录下的db.xls文件记录数据，自动登录www.yiwugou.com网站后台，发布产品数据

执行完成后生成db_result.xls文件，记录操作结果

"""

import string,thread,time
import xlrd
import xlwt
import sys

from xlutils.copy import copy

import json
import os
from Cjx.http import httpCient
__author__ = 'appie'

isExit = False
http = None
#用于显示状态的列的位置 一般是最后一列
cellStatusNum = 12

xmlWrite = None
xmlWriteTable = None
file = "db.xls"
result_file = "db_result.xls"
charset = "utf8"


def isNum(value):
    try:
        x = value + 1
    except TypeError:
        return False
    else:
        return True

def mPrint(msg):
    print msg.decode("utf8").encode(charset)


def login(username,password):
    loginurl = "http://cas.yiwugou.com/cas/login?p=ywg&service=http://work.yiwugou.com/casloginprocess.htm?done=http://www.yiwugou.com/"
    mPrint("正在登录，获取加密串 ...")
    content = http.urlcontent(loginurl)
    #print content
    item = http.itemDetail(content,{"lt":"lt\"\ value=\"(.*?)\"","execution":"execution\"\ value=\"(.*?)\""})
    mPrint("获取加密串:%s,密匙:%s"%(item['lt'],item['execution']))
    #loginurl = "http://cas.yiwugou.com/cas/login?p=ywg&service=http://work.yiwugou.com/casloginprocess.htm"
    #loginurl = "http://cas.yiwugou.com/cas/login?p=ywg"
    mPrint("正在登录 %s(%s) ..." % (username, password))
    param = {
        "lt":item['lt'],
        "execution":item['execution'],
        "_eventId":"submit",        
        "done":"http://www.yiwugou.com/",
        "username":username,
        "password":password,
        "rememberMe":1
    }
    #登录
    content = http.urlcontent(loginurl, param)
    #print content
    #mPrint(content)
    if not content or content.find("登录_义乌购") > 0:
        msg = "用户 %s(%s)登录失败" % (username, password)
        mPrint(msg)
        return [False, msg]
    return [True, ""]


def upload(img):
    '''
    上传图片
    :param img: 要上传的图片名称 函数里面会自动加上pic/的路径
    :return: 返回数组 arr[0]标志上传状态 成功：arr[1]上传成功的小图 上传成功的大图（原始图） 失败:arr[1]为具体的错误信息
    '''
    mPrint("正在上传图片 %s"%img)
    img = "pic/"+img
    if os.path.isfile(img) == False:
        msg ="图片 %s 不存在"%img
        mPrint(msg)
        return [False,msg]
    url = "http://upload2.yiwugou.com/upload/UploadServlet?fourpages=1&"
    try:
        param = {"a": open(img, "rb"),"callurl":"http://work.yiwugou.com/product/obj.htm?pindex=0&"}
    except:
        msg ="打开图片 %s 失败"%img
        mPrint(msg)
        return [False,msg]
    content = http.upload(url, param)
    if not content:
        msg = "图片 %s 上传失败"%img
        mPrint(msg)
        return msg
    content = content.encode("utf8")
    content = content.replace("<script language=\"javascript\" type=\"text/javascript\">window.location.href='http://work.yiwugou.com/product/obj.htm?pindex=0&?obj=","")
    content = content.replace("'</script>", "")
    try:
        jsonObj = json.loads(content,"utf8")
        originalpic = jsonObj["originalpic"].encode("utf8")
        smallpic = jsonObj["smallpic"].encode("utf8")

        if not originalpic:originalpic = ""
        if not smallpic:smallpic = ""

        #print jsonObj
    except:
        msg = "图片 %s 上传失败,获取图片返回数据失败"%img
        mPrint(msg)
        return [False,msg]
    return [True, smallpic, originalpic]

def post(title, pic, brief, desc, num, weight, biaoqian):
    mPrint("正在提交产品数据 %s(%d,%s)" % (title, num, weight))
    url = "http://work.yiwugou.com/product/supinsert.htm"
    param = {"productaction":"http://work.yiwugou.com/product/supinsert.htm",
             "categorystauts":"1",
             "title":title[0:300],
             "tag1":biaoqian,"tag2":"","tag3":"","tag4":"",   
             "image1":"http://images.onccc.com/%s"%pic[0],         
             "smallpic1":pic[0],
             "image2":"http://images.onccc.com/%s"%pic[1],
             "smallpic2":pic[1],
             "image3":"http://images.onccc.com/%s"%pic[2],
             "smallpic3":pic[2],
             "image4":"http://images.onccc.com/%s"%pic[3],
             "smallpic4":pic[3],
             "image3d":"",
             "proimg3d":1,
             "3dimgid":"",
             "callurlback":"http://work.yiwugou.com/product/obj.htm",
             "callurl":"http://work.yiwugou.com/product/obj.htm",
             "237":"",
             "238":"",
            "239":"",
            "386":"",
            "387":"",
            "388":"",
            "metric":"个",
             "customMetric":"个",
             "salenum":str(num),
             # "freight":weight,
             # "sellPrice":"",
             "price1":"1",
             "price00":"",
             "price03":"",
             "price10":"",
             "price13":"",
             "price20":"",
             "price23":"",
             "perentValue":"尺码",
             "perentValue":"颜色",
            "perentValue":"自定义",
            "perentValue":"自定义",
            "perentValue":"自定义",
            "brief":desc,
             "introduction":desc,             
             "freightTemplateIdradio":"3",
             "freightTemplateId":"4040",
             "freedelivery":"",
             "freight":weight,  
             "goodsflag":"1",
             "barcode":""             
    }
    #print param
    content = http.urlcontent(url, param,{"Referer":"http://work.yiwugou.com/product/product_post.htm","Content-Type":"application/x-www-form-urlencoded","User-Agent":"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36"})
    #print content
    if not content or content.find("成功") == -1:
        msg = "提交 %s 产品数据失败"%title
        mPrint(msg)
        return [False, msg]
    return [True, ""]

def showResult(row, errmsg=None):
    try:
        if not errmsg:
            xmlWriteTable.write(row, cellStatusNum, u"操作成功")
        else:
            xmlWriteTable.write(row, cellStatusNum, u"操作失败")
            xmlWriteTable.write(row, cellStatusNum+1, errmsg.decode("utf8"))
    except:
        mPrint("结果写数据表失败，行:%d"%row)

def showmsg(msg):
    msg = msg+"(按回车键退出)"
    mPrint(msg)
    raw_input()

def stringByCell(table,row,col,fieldName, need=True):
    try:
        r = table.row_values(row)
        tmp = r[col]
        if isNum(tmp):
            value = str(int(tmp))
        else:
            if len(tmp)<1:
                if need:
                    showResult(row, fieldName+"不能为空")
                return False
            else:
                value = tmp.encode("utf8")
        return value
    except:
        if need:
            showResult(row, fieldName+"操作失败")
        return False

def intByCell(table,row,col,fieldName,need=True):
    try:
        r = table.row_values(row)
        tmp = r[col]
        if not isNum(tmp):
            if need:
                showResult(row, fieldName+"必须为数字")
                return False
            else:
                return 0
        value = int(tmp)
        if need:
            if not value or value<1:
                showResult(row, fieldName+"必须大于0")
                return False
        return value
    except:
        showResult(row, fieldName+"操作失败")
        return False


def main():

    global http
    lastUsername = lastPassword = None

    #http = httpCient(False,None,"127.0.0.1:8888")
    http = httpCient()
    global charset
    charset = sys.getfilesystemencoding()

    if not os.path.isfile(file):
        showmsg("数据库文件 %s 在当前目录不存在，请确认" % file)
        return
    try:
        xls = xlrd.open_workbook(file)
        table = xls.sheets()[0]
    except:
        showmsg("打开数据库文件 %s 失败，请确认有操作权限" % file)
        return

    global xmlWrite,xmlWriteTable
    try:
        xmlWrite = copy(xls)
        xmlWriteTable = xmlWrite.get_sheet(0)
    except:
        showmsg("创建数据库读写对象失败，请确认数据表格式")
        return
    nrows = table.nrows
    if nrows<=1:
        showmsg("没有找到可操作的记录，自动退出")
        return
    succ = 0
    for i in range(nrows ):
        #row = table.row_values(i)
        # status = row[cellStatusNum]
        # if len(status)>0:
        #     continue
        #跳过第一行
        if i==0:continue
        #0为序号，直接跳过
        username = stringByCell(table,i,1,"用户名")
        if not username:continue

        password = stringByCell(table,i,2,"密码")
        if password == False:continue

        title = stringByCell(table,i,3,"标题")
        if not title:continue

        biaoqian = stringByCell(table,i,4,"标签")
        if not biaoqian:continue

        pic = stringByCell(table,i,5,"图片1")
        if not pic:continue

        pic2 = stringByCell(table,i,6,"图片2", False)
        pic3 = stringByCell(table,i,7,"图片3", False)
        pic4 = stringByCell(table,i,8,"图片4", False)

        desc = stringByCell(table,i,9,"描述")
        if not desc:continue


        num = intByCell(table,i,10,"数量")
        if not num:continue

        weight = intByCell(table,i,11,"重量", False)
        if not weight:
            weight=""
        else:
            weight = str(weight)

        #判断是否需要登录 上一次成功的用户密码不一样，则要登录
        if lastUsername != username or lastPassword != password:
            result = login(username, password)
            if not result[0]:
                showResult(i, result[1])
                continue
            lastUsername = username
            lastPassword = password

        #上传每一个图片 第一个图片必须
        result = upload(pic)
        #result = [True,"i003/2014/08/03/85/small_c5ca221a97aed66a22e7f9db84c9fe61.png","i003/2014/08/03/85/small_c5ca221a97aed66a22e7f9db84c9fe61.png"]
        if not result[0]:
            showResult(i, result[1])
            continue
        smallpic = result[1]
        originalpic = result[2]
        desc = desc + "<br /><img src=\"http://images.onccc.com/"+originalpic+"\" />"

        smallpic2 = ""
        if pic2:
            result = upload(pic2)
            if result[0]:
                smallpic2 = result[1]
                originalpic = result[2]
                desc = desc + "<br /><img src=\"http://images.onccc.com/"+originalpic+"\" />"

        smallpic3 = ""
        if pic3:
            result = upload(pic3)
            if result[0]:
                smallpic3 = result[1]
                originalpic = result[2]
                desc = desc + "<br /><img src=\"http://images.onccc.com/"+originalpic+"\" />"

        smallpic4 = ""
        if pic4:
            result = upload(pic4)
            if result[0]:
                smallpic4 = result[1]
                originalpic = result[2]
                desc = desc + "<br /><img src=\"http://images.onccc.com/"+originalpic+"\" />"


        result = post(title, [smallpic, smallpic2, smallpic3, smallpic4], title, desc, num, weight, biaoqian)
        if not result[0]:
            showResult(i, result[1])
            continue
        showResult(i)
        succ = succ + 1
    #table.flush_row_data()
    #xls.save(file)
    try:
        if os.path.isfile(result_file):
            os.remove(result_file)

        xmlWrite.save(result_file)
    except:
        showmsg("写入结果数据失败")
    showmsg("所有操作已经完成，共有记录 %d 条，成功上传 %d 条，结果保存在 %s 文件中"% (nrows-1,succ,result_file))


if __name__ == '__main__':
    main()

