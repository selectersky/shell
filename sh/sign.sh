#!/bin/sh
#brew install p7zip

method=$1
ipa=$2
profile=$3
versioncode=$4
versionname=$5

teamname="iPhone Distribution: Hangzhou Hikvision Digital Technology Co., Ltd."

# 脚本目录
root=$(cd `dirname $0`; pwd)
temp_path=./___sign_tmp
temp_entitlements_name="entitlements.txt"

if [ "$ipa" == "" -o "$ipa" == "-h" ]; then
	echo "查看ipa签名或执行签名工具 \n查看签名信息：Usage: sign r x.<ipa|mobileprovision|apk>\n执行签名：sign w x.ipa <profile_path> [内部版本号] [外部版本号]"
	exit 1
fi

if [ ! -f "$ipa" ]; then
	echo "路径不存在:$ipa"
	exit 1
fi

name=${ipa%.*}
ext=${ipa##*.}

if [ "$ext" == "apk" ]; then
	aapt dump badging $ipa
	exit 0
fi

if [ "$ext" == "ipa" ]; then
	rm -rf $temp_path
	7z x -bd -y -aot -bso0 -o$temp_path "${ipa}" 
	applicationName=$(ls -1 "$temp_path/Payload" | grep ".*\.app$" | head -n1)

	app_path="${temp_path}/Payload/${applicationName}"
	plist_path="${app_path}/embedded.mobileprovision"

else
	mkdir $temp_path
	plist_path=$ipa
fi

if [ "$method" == "w" ]; then
	if [ ! -f "$profile" ]; then
		echo "路径不存在:$profile"
		exit 1
	fi
	other_args=""
	if [ "$versioncode" != "" ]; then
		other_args="--bundle_version $versioncode"
	fi

	if [ "$versionname" != "" ]; then
		other_args="$other_args --short_version $versionname"
	fi
	echo $ipa,$profile,$other_args,${teamname},$app_path
	rm -rf $temp_entitlements_name
	codesign -d --entitlements :- "${app_path}" > $temp_entitlements_name
	cat $temp_entitlements_name
	fastlane sigh resign "${ipa}" --entitlements $temp_entitlements_name --signing_identity "${teamname}" -p "$profile" ${other_args}
	exit 0
fi



decode_plist=$temp_path/decode.plist

security cms -D -i "$plist_path" -o $decode_plist 2>/dev/null
ExpirationDate=`/usr/libexec/PlistBuddy -c "Print ExpirationDate" "$decode_plist"`
application_identifier=`/usr/libexec/PlistBuddy -c "Print :Entitlements:application-identifier" "$decode_plist"`
Name=`/usr/libexec/PlistBuddy -c "Print Name" "$decode_plist"`
TeamName=`/usr/libexec/PlistBuddy -c "Print TeamName" "$decode_plist"`
UUID=`/usr/libexec/PlistBuddy -c "Print UUID" "$decode_plist"`
#ProvisionedDevicese=`/usr/libexec/PlistBuddy -c "Print ProvisionedDevicese" "$decode_plist"`
result="{\"ExpirationDate\":\"$ExpirationDate\",\"application-identifier\":\"$application_identifier\",\"Name\":\"$Name\",\"TeamName\":\"$TeamName\",\"UUID\":\"$UUID\"}"
rm -rf $temp_path
echo $result