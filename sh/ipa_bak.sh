#!/bin/sh

project_path=$1
scheme=$2
configuration=$3

if [ "$project_path" == "" ]; then
	echo "Usage: ipa <.xcworkspace|.xcodeproj|.app|.xcarchive> [Scheme] [Release/Debug]"
	exit 1
fi

if [ ! -d "$project_path" ]; then
	echo "$project_path 路径不存在"
	exit 1
fi

project_name=${project_path##*/}
name=${project_name%"."*}
ext=${project_name#*"."}
pwd_path=`pwd`
ipa="${pwd_path}/$name.ipa"
buildpath='${pwd_path}/buildPath'
deviceData='${pwd_path}/deviceData'

project_para="-workspace \"$project_path\""
if [ "$ext" != "xcodeproj" ]; then
	project_para="-project \"$project_path\""
fi
if [ "$ext" == "xcarchive" ]; then
	xcodebuild -exportArchive -exportFormat "ipa" -archivePath $project_path -exportPath "$ipa" -exportProvisioningProfile 'f0b82c1a-970a-4fa6-8ad7-0b8bd13e4238' || exit 1
	exit 0
fi
if [ "$ext" == "app" ]; then
	tmp_path="$pwd_path/___ipa_tmp/"

	if [ -d "$tmp_path" ]; then
		rm -rf $tmp_path
	fi
	payload=$tmp_path/Payload/
	mkdir -p $payload
	cp -r $project_path "$payload"
	cd $tmp_path
	zip -rq $ipa *
	rm -rf $tmp_path
	exit 0
fi
if [ "$scheme" == "" ]; then
	echo "第二个参数target必须指定"
	exit 1
fi

if [ "$configuration" == "" ]; then
	configuration="Release"
	exit 1
fi
xcodebuild $project_para -scheme "$scheme" -configuration "$configuration" CONFIGURATION_BUILD_DIR="$buildpath" -derivedDataPath "$deviceData" || exit 1


#xcodebuild -exportArchive -exportFormat IPA -archivePath MyMobileApp.xcarchive -exportPath
#              MyMobileApp.ipa -exportProvisioningProfile 'MyMobileApp Distribution Profile'