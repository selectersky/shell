#!/bin/sh

if [ $# -ne 2 ]
then
  echo "Usage: md2doc xx.md xx.docx"
  exit 1
fi
if [ ! -f "$1" ]; then
	echo "markdown:${1} not exists"
	exit 1
fi
pandoc -s --highlight-style=tango "$1" -o "$2"